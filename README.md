**BlexSpunven**
===================


Es un sistema basico de punto de ventas escrito en Java.

--------
**Funciones disponibles:**

 - **Instalador** ~ Implementa un instalador sencillo automatico.
 - **Ventas** ~ Registra la ventas de tus productos
 - **Productos** ~ Lleva un registro de tus productos en stock, precio, y maneja las alertas para productos con stock bajo
 - **Sistema de empleados** ~ Dele a cada empleado su propio usuario y contraseña, todo lo que haga quedará bajo su nombre
 - **Sistema de proveedores** ~ Lleve el contro de los proveedores, como contacto y llegadas a repartir producto
 - **Sistema de rangos** ~ Separé las secciones de la aplicación mediante el sistema de rangos, permita que solo se vea una parte de ella

--------

**Nota:** *Se irá añadiento más funciones conforme se vaya avanzando en su desarrollo, por el momento solo cuenta con algunos metodos.*



--------
 BXSpunven | Sistema básico

 Sistema de punto de venta escrito en java

 Author: @BlackBlex (BlackBlex)

 License: General Public License (GPLv3) | http://www.gnu.org/licenses/
