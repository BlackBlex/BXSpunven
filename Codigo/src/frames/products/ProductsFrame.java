package frames.products;

import frames.products.ProductsModifyFrame;
import utils.objects.Category;
import bin.Main;
import com.blackblex.libs.application.components.styles.JButtonStyleFlat;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;
import com.blackblex.libs.system.utils.Observable;
import com.blackblex.libs.system.utils.Observer;
import com.blackblex.libs.system.utils.table.TableCell;
import frames.actions.MainFrame;
import utils.objects.Products;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import utils.ProductsModel;

public class ProductsFrame extends javax.swing.JFrame implements Observer {
    
    private List itemsProducts;
    private ProductsModel model;
    
    private int page = 1;
    private final String totalProducts = Main.config.getString("max_products_view");
    private int totalCountProducts;
    
    private Products selectProduct = null;
    
    public Observable observable = new Observable() {
        @Override
        public void action() {
            
        }
    };
    
    private final String format = "<html><strong>{desc} ~ {presentation} [{model}]</strong><br>" +
                "<table>" + 
                "<tr>" + 
                "<td style=\"width: 120px;\"><b>Codigo:</b>&nbsp;{id}</td>" +
                "<td style=\"width: 120px;\"><b>Precio:</b>&nbsp;${price}</td>" +
                "</tr>" + 
                "<tr>" + 
                "<td style=\"width: 120px;\"><b>Categoria:</b>&nbsp;{cat}</td>" +
                "<td style=\"width: 120px;\"><b>Stock:</b>&nbsp;{stock}</td>" +
                "</tr>" +
                "<tr>" + 
                "<td><b>Marca:</b>&nbsp;{mark}</td>" +
                "</tr>" +
                
                "</table>" +
                "</html>";
    
    Map<String, String> args = new HashMap<>();
    
    public ProductsFrame() {
        initComponents();
                
        JButtonStyleFlat jbutton3 = new JButtonStyleFlat(jButtonAdd, Main.buttonColorAccept2, 1);
        JButtonStyleFlat jbutton4 = new JButtonStyleFlat(jButtonDelete, Main.buttonColorDecline2, 1);
                
        jButtonAdd.setUI(jbutton3);
        jButtonDelete.setUI(jbutton4);
        
        jButtonDelete.setEnabled(false);
        
        this.args.put("{desc}","getDescription");
        this.args.put("{presentation}","getPresentation");
        this.args.put("{model}","getModel");
        this.args.put("{id}","getID");
        this.args.put("{price}","getPrice");
        this.args.put("{stock}","getStock");
        this.args.put("{cat}","getCategory");
        this.args.put("{mark}","getTrademark");
        
        jTableProducts.setDefaultRenderer(Products.class, new TableCell<>(this.args, this.format));
        jTableProducts.setDefaultEditor(Products.class,  new TableCell<>(this.args, this.format));
        jTableProducts.setRowHeight(90);
        
        jTableProducts.setShowGrid(false);
        jTableProducts.setRowMargin(4);
        
        loadProducts();
        this.setLocationRelativeTo(null);
        this.setTitle(Main.nameApp + " ~ Productos");
    }
    
    public void loadProducts() {
        OperationSQL sql = Main.SQL;

        itemsProducts = new ArrayList();
        
        sql.rows.put("id", "");
        MultiString[] result = sql.select("products");
        
        if ( result != null )
        {
            for (MultiString r : result) {
                Products i = new Products(r.getOne());
                i.get();
                itemsProducts.add(i);
            }

            jTextFieldFirst.setText(this.page + "");
            totalCountProducts = itemsProducts.size();
            jTextFieldSecond.setText(Integer.parseInt(Double.toString(Math.ceil((double) totalCountProducts/Double.parseDouble(totalProducts))).replace(".0", "")) + "");

            setModel();
            this.observable.notifyObs();
        }
    }
    
    public void setModel()
    {
        List items = new ArrayList();
        
        jTextFieldFirst.setText(this.page + "");
        int counter = 0;
        if (this.page == 1)
            counter = 0;
        else
            counter = ((this.page - 1)*Integer.parseInt(totalProducts));
        
        for ( int i = 0; i < (this.page*Integer.parseInt(totalProducts)); i++ )
        {
            if ( i >= counter )
            {
                try
                {
                    items.add(itemsProducts.get(i));
                }
                catch(Exception e){
                    
                }
            }
        }
                
        model = new ProductsModel(items);
        jTableProducts.setModel(model);
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelContent = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldSearch = new javax.swing.JTextField();
        jScrollPaneProducts = new javax.swing.JScrollPane();
        jTableProducts = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jTextFieldSecond = new javax.swing.JTextField();
        jLabelDe = new javax.swing.JLabel();
        jTextFieldFirst = new javax.swing.JTextField();
        jButtonPrevious = new javax.swing.JButton();
        jButtonNext = new javax.swing.JButton();
        jComboBoxOptions = new javax.swing.JComboBox<>();
        jButtonAdd = new javax.swing.JButton();
        jButtonDelete = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new javax.swing.OverlayLayout(getContentPane()));

        jPanelContent.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/search.png"))); // NOI18N
        jLabel1.setText("Buscar:");

        jTextFieldSearch.setToolTipText("");
        jTextFieldSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextFieldSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldSearchKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldSearchKeyTyped(evt);
            }
        });

        jTableProducts.setBackground(new java.awt.Color(102, 102, 102));
        jTableProducts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableProducts.setToolTipText("Doble click para editar el producto");
        jTableProducts.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableProductsMouseClicked(evt);
            }
        });
        jScrollPaneProducts.setViewportView(jTableProducts);

        jPanel1.setOpaque(false);
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jTextFieldSecond.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldSecond.setEnabled(false);
        jTextFieldSecond.setFocusable(false);
        jTextFieldSecond.setMinimumSize(new java.awt.Dimension(35, 35));
        jTextFieldSecond.setPreferredSize(new java.awt.Dimension(35, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel1.add(jTextFieldSecond, gridBagConstraints);

        jLabelDe.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelDe.setText("de");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 20;
        jPanel1.add(jLabelDe, gridBagConstraints);

        jTextFieldFirst.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldFirst.setEnabled(false);
        jTextFieldFirst.setFocusable(false);
        jTextFieldFirst.setMinimumSize(new java.awt.Dimension(35, 35));
        jTextFieldFirst.setPreferredSize(new java.awt.Dimension(35, 35));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        jPanel1.add(jTextFieldFirst, gridBagConstraints);

        jButtonPrevious.setText("<");
        jButtonPrevious.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPreviousActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jButtonPrevious, gridBagConstraints);

        jButtonNext.setText(">");
        jButtonNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNextActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanel1.add(jButtonNext, gridBagConstraints);

        jComboBoxOptions.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nombre", "Presentación", "Stock", "Codigo" }));

        jButtonAdd.setText("Agregar");
        jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddActionPerformed(evt);
            }
        });

        jButtonDelete.setText("Eliminar");
        jButtonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelContentLayout = new javax.swing.GroupLayout(jPanelContent);
        jPanelContent.setLayout(jPanelContentLayout);
        jPanelContentLayout.setHorizontalGroup(
            jPanelContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelContentLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelContentLayout.createSequentialGroup()
                        .addComponent(jButtonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPaneProducts, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelContentLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextFieldSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBoxOptions, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanelContentLayout.setVerticalGroup(
            jPanelContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelContentLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jComboBoxOptions, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTextFieldSearch, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPaneProducts, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11))
        );

        getContentPane().add(jPanelContent);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void buscar()
    {
        List items = new ArrayList();
        for ( Object product : itemsProducts)
        {
            Products pro = (Products) product;
            //matchesPattern(Pattern.compile(jTextField1.getText().toLowerCase()), pro.getDescription().toLowerCase())
            switch(jComboBoxOptions.getSelectedItem().toString())
            {
                case "Nombre":
                    if ( pro.getDescription().toLowerCase().startsWith(jTextFieldSearch.getText().toLowerCase()) )
                    {
                        items.add(product);
                    }
                    break;
                case "Presentación":
                    if ( pro.getPresentation().toLowerCase().startsWith(jTextFieldSearch.getText().toLowerCase()) )
                    {
                        items.add(product);
                    }
                    break;
                case "Codigo":
                    if ( pro.getID().startsWith(jTextFieldSearch.getText().toLowerCase()) )
                    {
                        items.add(product);
                    }
                    break;
                case "Stock":
                    if ( !jTextFieldSearch.getText().isEmpty() )
                        try {
                            
                            if ( pro.getStock() <= Integer.parseInt(jTextFieldSearch.getText()) )
                            {
                                items.add(product);
                            }
                        }
                        catch(Exception e)
                        {
                            
                        }
                    break;
            }
            model = new ProductsModel(items);
            jTableProducts.setModel(model);
        }
        
    }
    
    private void jTextFieldSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldSearchKeyPressed
        buscar();
        buscar();
    }//GEN-LAST:event_jTextFieldSearchKeyPressed

    private void jTextFieldSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldSearchKeyReleased
        buscar();
        buscar();
    }//GEN-LAST:event_jTextFieldSearchKeyReleased

    private void jTextFieldSearchKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldSearchKeyTyped
        buscar();
        buscar();
    }//GEN-LAST:event_jTextFieldSearchKeyTyped

    private void jButtonNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNextActionPerformed
        if ( (this.page+1) <= Integer.parseInt(jTextFieldSecond.getText()) )
            this.page++;
        setModel();
    }//GEN-LAST:event_jButtonNextActionPerformed

    private void jBButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBButton1ActionPerformed
        
    }//GEN-LAST:event_jBButton1ActionPerformed

    private void jButtonDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteActionPerformed
        if ( selectProduct != null )
            selectProduct.delete();
    }//GEN-LAST:event_jButtonDeleteActionPerformed

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddActionPerformed
        ProductsAddFrame prod = new ProductsAddFrame();
        prod.observable.add(this);
        prod.setVisible(true);
    }//GEN-LAST:event_jButtonAddActionPerformed

    private void jTableProductsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableProductsMouseClicked
        
        if (evt.getClickCount() == 2) {
            Products pro = (Products) model.getValueAt(jTableProducts.rowAtPoint(evt.getPoint()), jTableProducts.columnAtPoint(evt.getPoint()));

            ProductsModifyFrame prod = new ProductsModifyFrame(pro);
            prod.observable.add(this);
            prod.setVisible(true);
        }
        else
        {
            selectProduct = (Products) model.getValueAt(jTableProducts.rowAtPoint(evt.getPoint()), jTableProducts.columnAtPoint(evt.getPoint()));
            jButtonDelete.setEnabled(true);
        }
    }//GEN-LAST:event_jTableProductsMouseClicked

    private void jButtonPreviousActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPreviousActionPerformed
        if ( (this.page-1) >= 1 )
            this.page--;
        setModel();
    }//GEN-LAST:event_jButtonPreviousActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        MainFrame.setOpenFrame(false);
    }//GEN-LAST:event_formWindowClosing

    @Override
    public void update(Observable obj) {
        loadProducts();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonDelete;
    private javax.swing.JButton jButtonNext;
    private javax.swing.JButton jButtonPrevious;
    private javax.swing.JComboBox<String> jComboBoxOptions;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelDe;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JScrollPane jScrollPaneProducts;
    private javax.swing.JTable jTableProducts;
    private javax.swing.JTextField jTextFieldFirst;
    private javax.swing.JTextField jTextFieldSearch;
    private javax.swing.JTextField jTextFieldSecond;
    // End of variables declaration//GEN-END:variables

}
