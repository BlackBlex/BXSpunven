package frames.products;

import bin.Main;
import com.blackblex.libs.application.components.styles.JButtonStyleFlat;
import com.blackblex.libs.main.Init;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;
import com.blackblex.libs.system.utils.Observable;
import javax.swing.DefaultComboBoxModel;
import utils.objects.Category;
import utils.objects.Products;
import utils.objects.Trademark;

public class ProductsModifyFrame extends javax.swing.JFrame {

    public Observable observable = new Observable() {
        @Override
        public void action() {
            
        }
    };
    
    private final OperationSQL sql = Main.SQL;
    private Products product;
    DefaultComboBoxModel ComboBoxModelTrademarks, ComboBoxModelCategories;

    public ProductsModifyFrame(Products pro) {
        
        this.product = pro;
        
        initComponents();

        JButtonStyleFlat jbuttonaddproduct = new JButtonStyleFlat(this.jButtonAddProduct, Main.buttonColorAccept2, 0);
        this.jButtonAddProduct.setUI(jbuttonaddproduct);

        loadTrademarks();
        loadCategories();
                
        if ( this.product.getID().length() > 8 )
        {
            String id = this.product.getID().substring(8, this.product.getID().length());
            jTextFieldCodeProduct.setText(id);
        }
        else
            jTextFieldCodeProduct.setText(this.product.getID());

        jComboBoxCategory.getModel().setSelectedItem(this.product.getCategory());
        jComboBoxCategory.updateUI();
        
        jComboBoxTrademark.getModel().setSelectedItem(this.product.getTrademark());
        jComboBoxTrademark.updateUI();

        jTextFieldDesc.setText(this.product.getDescription());
        jTextFieldModel.setText(this.product.getModel());
        jTextFieldPresentation.setText(this.product.getPresentation());
        jTextFieldPrice.setText(Double.toString(this.product.getPrice()));
        jSliderStock.setValue(this.product.getStock());
        jLabel10.setText(jSliderStock.getValue() + "");
        
        this.setLocationRelativeTo(null);
        
        this.setTitle(Main.nameApp + " ~ Modificar producto");
    }

    public void loadTrademarks() {
        ComboBoxModelTrademarks = new DefaultComboBoxModel();
        ComboBoxModelTrademarks.addElement("Sin marca");
        sql.rows.put("id", "");
        MultiString[] tra = sql.select("trademark");
        if ( tra != null )
        {
            for (MultiString t : tra) {
                Trademark trad = new Trademark(Integer.parseInt(t.getOne()));
                trad.get();
                ComboBoxModelTrademarks.addElement(trad);
            }
            jComboBoxTrademark.setModel(ComboBoxModelTrademarks);
        }
    }

    public void loadCategories() {
        ComboBoxModelCategories = new DefaultComboBoxModel();
        ComboBoxModelCategories.addElement("Sin categoria");
        sql.rows.put("id", "");
        MultiString[] cat = sql.select("categories");
        if ( cat != null )
        {
            for (MultiString c : cat) {
                Category cate = new Category(Integer.parseInt(c.getOne()));
                cate.get();
                ComboBoxModelCategories.addElement(cate);
            }
            jComboBoxCategory.setModel(ComboBoxModelCategories);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabelCodeProduct = new javax.swing.JLabel();
        jTextFieldCodeProduct = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jComboBoxTrademark = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jComboBoxCategory = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldDesc = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldModel = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldPresentation = new javax.swing.JTextField();
        jTextFieldPrice = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jSliderStock = new javax.swing.JSlider();
        jLabel10 = new javax.swing.JLabel();
        jButtonAddProduct = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new javax.swing.OverlayLayout(getContentPane()));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Codigo del producto:");
        jLabel1.setPreferredSize(new java.awt.Dimension(150, 33));

        jLabelCodeProduct.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabelCodeProduct.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelCodeProduct.setText("0");
        jLabelCodeProduct.setPreferredSize(new java.awt.Dimension(121, 30));
        jLabelCodeProduct.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelCodeProductMouseClicked(evt);
            }
        });

        jTextFieldCodeProduct.setEditable(false);
        jTextFieldCodeProduct.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextFieldCodeProduct.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldCodeProduct.setPreferredSize(new java.awt.Dimension(130, 33));

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Marca:");
        jLabel3.setPreferredSize(new java.awt.Dimension(150, 33));

        jComboBoxTrademark.setEnabled(false);
        jComboBoxTrademark.setPreferredSize(new java.awt.Dimension(181, 33));
        jComboBoxTrademark.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxTrademarkItemStateChanged(evt);
            }
        });

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Categoria:");
        jLabel4.setPreferredSize(new java.awt.Dimension(150, 33));

        jComboBoxCategory.setPreferredSize(new java.awt.Dimension(181, 33));
        jComboBoxCategory.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxCategoryItemStateChanged(evt);
            }
        });

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Descripción:");
        jLabel5.setPreferredSize(new java.awt.Dimension(150, 33));

        jTextFieldDesc.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldDesc.setPreferredSize(new java.awt.Dimension(181, 33));

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Modelo:");
        jLabel6.setPreferredSize(new java.awt.Dimension(150, 33));

        jTextFieldModel.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldModel.setPreferredSize(new java.awt.Dimension(181, 33));

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Presentación:");
        jLabel7.setPreferredSize(new java.awt.Dimension(150, 33));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Precio:");
        jLabel8.setPreferredSize(new java.awt.Dimension(150, 33));

        jTextFieldPresentation.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldPresentation.setPreferredSize(new java.awt.Dimension(181, 33));

        jTextFieldPrice.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldPrice.setPreferredSize(new java.awt.Dimension(181, 33));

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Stock:");
        jLabel9.setPreferredSize(new java.awt.Dimension(150, 33));

        jSliderStock.setMaximum(200);
        jSliderStock.setMinimum(10);
        jSliderStock.setValue(10);
        jSliderStock.setOpaque(false);
        jSliderStock.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSliderStockStateChanged(evt);
            }
        });

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jButtonAddProduct.setText("Actualizar");
        jButtonAddProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddProductActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabelCodeProduct, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextFieldCodeProduct, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jComboBoxTrademark, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(jComboBoxCategory, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jTextFieldDesc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jTextFieldModel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jTextFieldPresentation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jTextFieldPrice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jSliderStock, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 23, Short.MAX_VALUE))
                    .addComponent(jButtonAddProduct, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelCodeProduct, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(2, 2, 2))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldCodeProduct, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(34, 34, 34)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxCategory, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxTrademark, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldDesc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldModel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldPresentation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jSliderStock, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addComponent(jButtonAddProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        getContentPane().add(jPanel1);

        setBounds(0, 0, 480, 528);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabelCodeProductMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelCodeProductMouseClicked
        jTextFieldCodeProduct.requestFocus();
    }//GEN-LAST:event_jLabelCodeProductMouseClicked

    private void jSliderStockStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSliderStockStateChanged
        jLabel10.setText(Integer.toString(jSliderStock.getValue()));
    }//GEN-LAST:event_jSliderStockStateChanged

    private void jComboBoxTrademarkItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxTrademarkItemStateChanged
        changeID();
    }//GEN-LAST:event_jComboBoxTrademarkItemStateChanged

    private void jComboBoxCategoryItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxCategoryItemStateChanged
        jComboBoxTrademark.setEnabled(true);
        Category cat = (Category) ComboBoxModelCategories.getElementAt(jComboBoxCategory.getSelectedIndex());
        loadTrademark(cat.getID());
        changeID();
    }//GEN-LAST:event_jComboBoxCategoryItemStateChanged

    private void jButtonAddProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddProductActionPerformed
        this.product.setID(this.jLabelCodeProduct.getText() + this.jTextFieldCodeProduct.getText());
        this.product.setPrice(Double.parseDouble(this.jTextFieldPrice.getText()));
        this.product.setDescription(this.jTextFieldDesc.getText());
        Category cat = (Category) ComboBoxModelCategories.getElementAt(jComboBoxCategory.getSelectedIndex());
        this.product.setCategory(cat);
        Trademark tra = (Trademark) ComboBoxModelTrademarks.getElementAt(jComboBoxTrademark.getSelectedIndex());
        this.product.setTrademark(tra);
        this.product.setModel(this.jTextFieldModel.getText());
        this.product.setPresentation(this.jTextFieldPresentation.getText());
        this.product.setStock(this.jSliderStock.getValue());

        if (this.product.save()) {
            Init.MSG.showMessageInformation("Producto actualizado exitosamente!");
            this.dispose();
            this.observable.notifyObs();
        }
        else
            Init.MSG.showMessageInformation("El producto no pudo ser actualizado!");
    }//GEN-LAST:event_jButtonAddProductActionPerformed

    public void loadTrademark(int category) {
        ComboBoxModelTrademarks = new DefaultComboBoxModel();
        ComboBoxModelTrademarks.addElement("Sin marca");
        sql.rows.put("id_trademark", "");
        sql.wheres.put(new MultiString("id_category", Integer.toString(category)), new MultiString("=", "AND"));
        sql.wheres.put(new MultiString("enabled", Integer.toString(1)), new MultiString("=", ""));
        MultiString[] trad = sql.select("categories_trademark");
        if ( trad != null )
        {
            for (MultiString t : trad) {
                Trademark tra = new Trademark(Integer.parseInt(t.getOne()));
                tra.get();
                ComboBoxModelTrademarks.addElement(tra);
            }
            jComboBoxTrademark.setModel(ComboBoxModelTrademarks);
        }
    }

    public void changeID() {
        Trademark trad = new Trademark(0);
        trad.setCode("0");
        Category cate = new Category(0);

        if (jComboBoxTrademark.getSelectedItem() != "Sin marca" && jComboBoxTrademark.isEnabled()) {
            trad = (Trademark) ComboBoxModelTrademarks.getElementAt(jComboBoxTrademark.getSelectedIndex());
        }
        if (jComboBoxCategory.getSelectedItem() != "Sin categoria") {
            cate = (Category) ComboBoxModelCategories.getElementAt(jComboBoxCategory.getSelectedIndex());
        }
        
        String tradeCode = "", cateCode = "";
        
        try
        {
            tradeCode = String.format("%0" + Main.config.getValue("max_leading_zeros_trademark") + "d", Integer.parseInt(trad.getCode()));
            cateCode = String.format("%03d", cate.getID());
        }
        catch ( Exception ex){
            
        }

        jLabelCodeProduct.setText("10" + cateCode + tradeCode);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddProduct;
    private javax.swing.JComboBox<String> jComboBoxCategory;
    private javax.swing.JComboBox<String> jComboBoxTrademark;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelCodeProduct;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSlider jSliderStock;
    private javax.swing.JTextField jTextFieldCodeProduct;
    private javax.swing.JTextField jTextFieldDesc;
    private javax.swing.JTextField jTextFieldModel;
    private javax.swing.JTextField jTextFieldPresentation;
    private javax.swing.JTextField jTextFieldPrice;
    // End of variables declaration//GEN-END:variables
}
