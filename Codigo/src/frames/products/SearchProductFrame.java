package frames.products;

import bin.Main;
import com.blackblex.libs.main.Init;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;
import com.blackblex.libs.system.utils.table.TableCell;
import com.blackblex.libs.system.utils.Observable;
import utils.objects.Products;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import utils.ProductsModel;
import utils.objects.Provider;

public class SearchProductFrame extends javax.swing.JFrame {
    
    public Observable observable = new Observable() {
        @Override
        public void action() {
            
        }
    };
    
    private List itemsProducts;
    private ProductsModel model;    
    private Provider provider;
        
    private final String format = "<html><strong>{desc} ~ {presentation} [{model}]</strong><br>" +
                "<table>" + 
                "<tr>" + 
                "<td style=\"width: 120px;\"><b>Codigo:</b>&nbsp;{id}</td>" +
                "<td style=\"width: 120px;\"><b>Precio:</b>&nbsp;${price}</td>" +
                "</tr>" + 
                "<tr>" + 
                "<td style=\"width: 120px;\"><b>Categoria:</b>&nbsp;{cat}</td>" +
                "<td style=\"width: 120px;\"><b>Stock:</b>&nbsp;{stock}</td>" +
                "</tr>" +
                "<tr>" + 
                "<td><b>Marca:</b>&nbsp;{mark}</td>" +
                "</tr>" +
                
                "</table>" +
                "</html>";
    
    Map<String, String> args = new HashMap<>();
        
    public SearchProductFrame() {
        initComponents();
        
        this.args.put("{desc}","getDescription");
        this.args.put("{presentation}","getPresentation");
        this.args.put("{model}","getModel");
        this.args.put("{id}","getID");
        this.args.put("{price}","getPrice");
        this.args.put("{stock}","getStock");
        this.args.put("{cat}","getCategory");
        this.args.put("{mark}","getTrademark");
        
        jTableProducts.setDefaultRenderer(Products.class, new TableCell<>(this.args, this.format));
        jTableProducts.setDefaultEditor(Products.class,  new TableCell<>(this.args, this.format));
        jTableProducts.setRowHeight(90);
        
        jTableProducts.setShowGrid(false);
        jTableProducts.setRowMargin(4);
        
        this.setLocationRelativeTo(null);
        this.setTitle(Main.nameApp + " ~ Buscar producto");
        
        loadProducts(false);
    }
    
    
    public SearchProductFrame(Provider provider) {
        initComponents();
        
        if ( provider != null )
            this.provider = provider;
        else
            this.provider = new Provider(0);
        this.args.put("{desc}","getDescription");
        this.args.put("{presentation}","getPresentation");
        this.args.put("{model}","getModel");
        this.args.put("{id}","getID");
        this.args.put("{price}","getPrice");
        this.args.put("{stock}","getStock");
        this.args.put("{cat}","getCategory");
        this.args.put("{mark}","getTrademark");
        
        jTableProducts.setDefaultRenderer(Products.class, new TableCell<>(this.args, this.format));
        jTableProducts.setDefaultEditor(Products.class,  new TableCell<>(this.args, this.format));
        jTableProducts.setRowHeight(90);
        
        jTableProducts.setShowGrid(false);
        jTableProducts.setRowMargin(4);
        
        this.setLocationRelativeTo(null);
        this.setTitle(Main.nameApp + " ~ Buscar producto");
        
        loadProducts(true);
    }
    
    public void loadProducts(boolean provider) {
        OperationSQL sql = Main.SQL;

        itemsProducts = new ArrayList();
        MultiString[] resultProTra = null;
        if ( provider )
        {
            sql.rows.put("id_trademark", "");
            sql.wheres.put(new MultiString("id_provider", Integer.toString(this.provider.getID())), new MultiString("=", ""));
            resultProTra = sql.select("providers_trademark");
        }
        
        sql.rows.put("id", "");
        if ( provider )
        {
            if ( resultProTra != null )
            {                
                for ( int i = 0; i < resultProTra.length; i++ )
                    if ( i == resultProTra.length-1 )
                        sql.wheres.put(new MultiString("id_trademark", resultProTra[i].getOne()), new MultiString("=", ""));
                    else
                        sql.wheres.put(new MultiString("id_trademark", resultProTra[i].getOne()), new MultiString("=", "OR"));
            }
                
        }
        MultiString[] result = sql.select("products");
        
        if ( result != null )
        {
            for (MultiString r : result) {
                Products i = new Products(r.getOne());
                i.get();
                itemsProducts.add(i);
            }

            setModel();
        }
        else
        {
            this.dispose();
            if ( provider )
                Init.MSG.showMessageError("No hay ningun producto asociado a este proveedor.");
        }
    }
    
    public void setModel()
    {
        List items = new ArrayList();
        
        for ( int i = 0; i < itemsProducts.size(); i++ )
        {
            items.add(itemsProducts.get(i));
        }
        model = new ProductsModel(items);
        jTableProducts.setModel(model);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelContent = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jScrollPaneProducts = new javax.swing.JScrollPane();
        jTableProducts = new javax.swing.JTable();
        jComboBox1 = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new javax.swing.OverlayLayout(getContentPane()));

        jPanelContent.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/search.png"))); // NOI18N
        jLabel1.setText("Buscar:");

        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField1KeyTyped(evt);
            }
        });

        jTableProducts.setBackground(new java.awt.Color(102, 102, 102));
        jTableProducts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableProducts.setToolTipText("Double click para agregar el producto a la lista");
        jTableProducts.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableProductsMouseClicked(evt);
            }
        });
        jScrollPaneProducts.setViewportView(jTableProducts);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nombre", "Presentación", "Stock", "Codigo" }));

        javax.swing.GroupLayout jPanelContentLayout = new javax.swing.GroupLayout(jPanelContent);
        jPanelContent.setLayout(jPanelContentLayout);
        jPanelContentLayout.setHorizontalGroup(
            jPanelContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelContentLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPaneProducts, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelContentLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanelContentLayout.setVerticalGroup(
            jPanelContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelContentLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanelContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTextField1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPaneProducts, javax.swing.GroupLayout.PREFERRED_SIZE, 392, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getContentPane().add(jPanelContent);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void buscar()
    {
        List items = new ArrayList();
        for ( Object product : itemsProducts)
        {
            Products pro = (Products) product;
            //matchesPattern(Pattern.compile(jTextField1.getText().toLowerCase()), pro.getDescription().toLowerCase())
            switch(jComboBox1.getSelectedItem().toString())
            {
                case "Nombre":
                    if ( pro.getDescription().toLowerCase().startsWith(jTextField1.getText().toLowerCase()) )
                    {
                        items.add(product);
                    }
                    break;
                case "Presentación":
                    if ( pro.getPresentation().toLowerCase().startsWith(jTextField1.getText().toLowerCase()) )
                    {
                        items.add(product);
                    }
                    break;
                case "Codigo":
                    if ( pro.getID().startsWith(jTextField1.getText().toLowerCase()) )
                    {
                        items.add(product);
                    }
                    break;
                case "Stock":
                    if ( !jTextField1.getText().isEmpty() )
                        try {
                            
                            if ( pro.getStock() <= Integer.parseInt(jTextField1.getText()) )
                            {
                                items.add(product);
                            }
                        }
                        catch(Exception e)
                        {
                            
                        }
                    break;
            }
            model = new ProductsModel(items);
            jTableProducts.setModel(model);
        }
        
    }
    
    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        buscar();
        buscar();
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jTextField1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyReleased
        buscar();
        buscar();
    }//GEN-LAST:event_jTextField1KeyReleased

    private void jTextField1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyTyped
        buscar();
        buscar();
    }//GEN-LAST:event_jTextField1KeyTyped

    private void jBButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBButton1ActionPerformed
        
    }//GEN-LAST:event_jBButton1ActionPerformed

    private void jTableProductsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableProductsMouseClicked
        if (evt.getClickCount() == 2) {
            Products pro = (Products) model.getValueAt(jTableProducts.rowAtPoint(evt.getPoint()), jTableProducts.columnAtPoint(evt.getPoint()));
            this.observable.addArgs(pro);
            this.observable.notifyObs();
            
            this.dispose();
        }        
    }//GEN-LAST:event_jTableProductsMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JScrollPane jScrollPaneProducts;
    private javax.swing.JTable jTableProducts;
    private javax.swing.JTextField jTextField1;
    // End of variables declaration//GEN-END:variables

}
