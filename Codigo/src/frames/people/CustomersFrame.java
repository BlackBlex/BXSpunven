package frames.people;

import bin.Main;
import com.blackblex.libs.application.components.styles.JButtonStyleFlat;
import com.blackblex.libs.main.Init;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;
import com.blackblex.libs.system.utils.table.TableCell;
import frames.actions.MainFrame;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JTabbedPane;
import utils.objects.Customer;
import utils.CustomerModel;

public class CustomersFrame extends javax.swing.JFrame {
        
    private List itemsCustomer;
    private CustomerModel model;
    
    private int page = 1;
    private final String totalCustomer = "10";
    private int totalCountCustomer;
    
    private final String format = "<html>"
            + "<table>"
            + "<tr>"
            + "<td><img src=\"file:{img}\" height=\"50\" width=\"50\"></td>"
            + "<td>"
            + "<table>" + 
                "<tr>" + 
                "<td style=\"width: 120px;\"><strong>{name}</strong></td>" +
                "</tr>" + 
                "<tr>" + 
                "<td style=\"width: 120px;\">{street}</td>" +
                "<td style=\"width: 120px;\">{tel}</td>" +
                "</tr>" + 
                "<tr>" + 
                "<td style=\"width: 120px;\">${spent}</td>" +
                "</tr>" +
                "</table>"
            + "</td>"
            + "</tr>"        
            + "</table>"
            + "</html>";
    
    Map<String, String> args = new HashMap<>();
    
    public CustomersFrame() {
        initComponents();
                
        this.args.put("{img}","getAvatar");
        this.args.put("{name}","getName");
        this.args.put("{street}","getStreet");
        this.args.put("{tel}","getTelephone");
        this.args.put("{spent}","getSpent");
        
        
        jTableCustomer.setDefaultRenderer(Customer.class, new TableCell<>(this.args, this.format));
        jTableCustomer.setDefaultEditor(Customer.class,  new TableCell<>(this.args, this.format));
        jTableCustomer.setRowHeight(90);
        
        jTableCustomer.setShowGrid(false);
        
        
        JButtonStyleFlat jbuttonaddcustomer = new JButtonStyleFlat(jButtonAddCustomer, Main.buttonColorAccept2, 3);
        jButtonAddCustomer.setUI(jbuttonaddcustomer);
        
        loadCustomers();
        this.setLocationRelativeTo(null);
        this.setTitle(Main.nameApp + " ~ Clientes");
    }
    
    
    public void loadCustomers() {
        OperationSQL sql = Main.SQL;

        itemsCustomer = new ArrayList();

        sql.rows.put("id", "");
        MultiString[] result = sql.select("customers");
        
        List items = new ArrayList();
        if ( result != null )
        {
            for (MultiString r : result) {
                Customer i = new Customer(Integer.parseInt(r.getOne()));
                i.get();
                itemsCustomer.add(i);
            }

            jTextFieldFirst.setText(this.page + "");
            totalCountCustomer = itemsCustomer.size();
            jTextFieldSecond.setText(Integer.parseInt(Double.toString(Math.ceil((double) totalCountCustomer/Double.parseDouble(totalCustomer))).replace(".0", "")) + "");

            setModel();
        }
    }
    
    public void setModel()
    {
        List items = new ArrayList();
        
        jTextFieldFirst.setText(this.page + "");
        int counter = 0;
        if (this.page == 1)
            counter = 0;
        else
            counter = ((this.page - 1)*Integer.parseInt(totalCustomer));
        
        for ( int i = 0; i < (this.page*Integer.parseInt(totalCustomer)); i++ )
        {
            if ( i >= counter )
            {
                try
                {
                    items.add(itemsCustomer.get(i));
                }
                catch(Exception e){
                    
                }
            }
        }
                
        model = new CustomerModel(items);
        jTableCustomer.setModel(model);
        
    }
    
    
    public void buscar()
    {
        List items = new ArrayList();
        for ( Object customer : itemsCustomer)
        {
            Customer cus = (Customer) customer;
            
            if ( cus.getName().toLowerCase().startsWith(jTextField1.getText().toLowerCase()))
                items.add(customer);
            
            model = new CustomerModel(items);
            jTableCustomer.setModel(model);
        }
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableCustomer = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jTextFieldFirst = new javax.swing.JTextField();
        jLabelDe = new javax.swing.JLabel();
        jTextFieldSecond = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jButtonAddCustomer = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldStreet = new javax.swing.JTextField();
        jTextFieldTelephone = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new javax.swing.OverlayLayout(getContentPane()));

        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jScrollPane1.setViewportView(jTableCustomer);

        jButton1.setText("<");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTextFieldFirst.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldFirst.setEnabled(false);
        jTextFieldFirst.setFocusable(false);
        jTextFieldFirst.setMinimumSize(new java.awt.Dimension(35, 35));
        jTextFieldFirst.setPreferredSize(new java.awt.Dimension(35, 35));

        jLabelDe.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelDe.setText("de");

        jTextFieldSecond.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldSecond.setEnabled(false);
        jTextFieldSecond.setFocusable(false);
        jTextFieldSecond.setMinimumSize(new java.awt.Dimension(35, 35));
        jTextFieldSecond.setPreferredSize(new java.awt.Dimension(35, 35));

        jButton2.setText(">");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField1KeyTyped(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/search.png"))); // NOI18N
        jLabel1.setText("Buscar:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addGap(5, 5, 5)
                        .addComponent(jTextFieldFirst, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jLabelDe, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jTextFieldSecond, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(jButton2)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldFirst, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabelDe))
                    .addComponent(jTextFieldSecond, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Lista de clientes", new javax.swing.ImageIcon(getClass().getResource("/resources/customerslist.png")), jPanel1); // NOI18N

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jButtonAddCustomer.setText("Agregar");
        jButtonAddCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddCustomerActionPerformed(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Nombre:");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Dirección:");

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Telefono:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonAddCustomer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextFieldTelephone, javax.swing.GroupLayout.DEFAULT_SIZE, 303, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldStreet)
                            .addComponent(jTextFieldName))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldName, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldStreet, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldTelephone, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jButtonAddCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(146, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Agregar cliente", new javax.swing.ImageIcon(getClass().getResource("/resources/customeradd.png")), jPanel2); // NOI18N

        getContentPane().add(jTabbedPane1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        if ( (this.page-1) >= 1 )
        this.page--;
        setModel();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        if ( (this.page+1) <= Integer.parseInt(jTextFieldSecond.getText()) )
        this.page++;
        setModel();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        //setModel();

        buscar();
        buscar();
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jTextField1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyReleased
        //setModel();
        buscar();
        buscar();
    }//GEN-LAST:event_jTextField1KeyReleased

    private void jTextField1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyTyped

        buscar();
        buscar();
    }//GEN-LAST:event_jTextField1KeyTyped

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
        JTabbedPane sourceTabbedPane = (JTabbedPane) evt.getSource();
        int index = sourceTabbedPane.getSelectedIndex();
        if (sourceTabbedPane.getTitleAt(index).equals("Lista de clientes"))
            this.setSize(this.getSize().width, 440);
        else
            this.setSize(this.getSize().width, 330);
        
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void jButtonAddCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddCustomerActionPerformed
        
        if (jTextFieldName.getText().isEmpty() || jTextFieldStreet.getText().isEmpty() || jTextFieldTelephone.getText().isEmpty())
            Init.MSG.showMessageError("No deje algún campo sin rellenar");
        else
        {
            Customer customer = new Customer(-1);
        
            customer.setName(jTextFieldName.getText());
            customer.setStreet(jTextFieldStreet.getText());
            customer.setTelephone(Long.parseLong(jTextFieldTelephone.getText()));
            customer.setSpent(0);

            if ( customer.save() )
            {
                Init.MSG.showMessageInformation("Cliente registrado");
                jTabbedPane1.setSelectedIndex(0);
                loadCustomers();
            }
            else
                Init.MSG.showMessageError("El cliente no pudo ser registrado");
        }
    }//GEN-LAST:event_jButtonAddCustomerActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        MainFrame.setOpenFrame(false);
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButtonAddCustomer;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabelDe;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTableCustomer;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextFieldFirst;
    private javax.swing.JTextField jTextFieldName;
    private javax.swing.JTextField jTextFieldSecond;
    private javax.swing.JTextField jTextFieldStreet;
    private javax.swing.JTextField jTextFieldTelephone;
    // End of variables declaration//GEN-END:variables
}
