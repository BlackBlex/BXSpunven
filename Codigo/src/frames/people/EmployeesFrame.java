package frames.people;

import bin.Main;
import com.blackblex.libs.application.components.styles.JButtonStyleFlat;
import com.blackblex.libs.main.Init;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;
import com.blackblex.libs.system.utils.Observable;
import com.blackblex.libs.system.utils.Observer;
import com.blackblex.libs.system.utils.Security;
import com.blackblex.libs.system.utils.table.TableCell;
import frames.actions.MainFrame;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import utils.objects.Employee;
import utils.EmployeeModel;
import utils.objects.Rank;

public class EmployeesFrame extends javax.swing.JFrame implements Observer {
    
    private List itemsEmployee, itemsRank;
    private EmployeeModel model;
    
    private Employee employeeNow = null;
    
    private DefaultTableModel jtableRankModel;
    
    private DefaultComboBoxModel ComboBoxModelRanks;
    
    private final OperationSQL sql = Main.SQL;
    
    private int page = 1;
    private final String totalEmployee = "5";
    private int totalCountEmployee;
    
    private final String format = "<html>"
            + "<table>"
            + "<tr>"
            + "<td><img src=\"file:{img}\" height=\"50\" width=\"50\"></td>"
            + "<td>"
            + "<table>" + 
                "<tr>" + 
                "<td style=\"width: 120px;\"><strong>{name} {lastname}</strong></td>" +
                "</tr>" + 
                "<tr>" + 
                "<td style=\"width: 120px;\">{user}</td>" +
                "<td style=\"width: 120px;\">{email}</td>" +
                "</tr>" + 
                "<tr>" + 
                "<td style=\"width: 120px;\">{rank}</td>" +
                "</tr>" +
                "</table>"
            + "</td>"
            + "</tr>"        
            + "</table>"
            + "</html>";
    
    Map<String, String> args = new HashMap<>();
    
    public EmployeesFrame() {
        initComponents();
                
        this.args.put("{img}","getAvatar");
        this.args.put("{name}","getName");
        this.args.put("{lastname}","getLastname");
        this.args.put("{user}","getUser");
        this.args.put("{email}","getEmail");
        this.args.put("{rank}","getRankName");
        
        
        jTableEmployee.setDefaultRenderer(Employee.class, new TableCell<>(this.args, this.format));
        jTableEmployee.setDefaultEditor(Employee.class,  new TableCell<>(this.args, this.format));
        jTableEmployee.setRowHeight(90);
        
        jTableEmployee.setShowGrid(false);
        
        
        JButtonStyleFlat jbuttonaddcustomer = new JButtonStyleFlat(jButtonAddEmployee, Main.buttonColorAccept2, 3);
        jButtonAddEmployee.setUI(jbuttonaddcustomer);
        
        loadEmployees();
        loadRanks();
        this.setLocationRelativeTo(null);
        this.setTitle(Main.nameApp + " ~ Empleados");
        
        /*
            Rank tab
        */
        
        jTableRanks.setModel(jtableRankModel);
        TableCellRenderer rendererFromHeader = jTableRanks.getTableHeader().getDefaultRenderer();
        JLabel headerLabel = (JLabel) rendererFromHeader;
        headerLabel.setHorizontalAlignment(JLabel.CENTER);
        
        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
        jTableRanks.getColumnModel().getColumn(0).setCellRenderer(dtcr);
        jTableRanks.getColumnModel().getColumn(1).setCellRenderer(dtcr);
        jTableRanks.setAutoCreateRowSorter(true);
        jTableRanks.getTableHeader().setReorderingAllowed(false);
        jTableRanks.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTableRanks.setRowHeight(35);
                
        
        JButtonStyleFlat jbuttonadd = new JButtonStyleFlat(jButton3, Main.buttonColorAccept, 3);
        jButton3.setUI(jbuttonadd);

        JButtonStyleFlat jbuttoneditrank = new JButtonStyleFlat(jButtonEditRank, Main.buttonColorSearch, 3);
        jButtonEditRank.setUI(jbuttoneditrank);
        
        /*
            Rank tab
        */
        
    }
    
    public void loadRanks()
    {
        itemsRank = new ArrayList();
        
        sql.rows.put("id", "");
        MultiString[] ranks = sql.select("ranks");
        if ( ranks != null )
        {
            for (MultiString rank : ranks) {
                Rank ran = new Rank(Integer.parseInt(rank.getOne()));
                ran.get();
                itemsRank.add(ran);
            }
            refreshTableRank();
            refreshComboRank();
        }
    }
        
    public void loadEmployees() {
        
        itemsEmployee = new ArrayList();

        sql.rows.put("id", "");
        MultiString[] result = sql.select("employees");
        if ( result != null )
        {
            for (MultiString r : result) {
                Employee i = new Employee(Integer.parseInt(r.getOne()));
                i.get();
                itemsEmployee.add(i);
            }

            jTextFieldFirst.setText(this.page + "");
            totalCountEmployee = itemsEmployee.size();
            jTextFieldSecond.setText(Integer.parseInt(Double.toString(Math.ceil((double) totalCountEmployee/Double.parseDouble(totalEmployee))).replace(".0", "")) + "");

            setModel();
        }
    }
    
    public void refreshComboRank()
    {
        ComboBoxModelRanks = new DefaultComboBoxModel();
        ComboBoxModelRanks.addElement("Sin rango");
        for ( Object rank : itemsRank )
            ComboBoxModelRanks.addElement((Rank) rank);
        
        jComboBoxRank.setModel(ComboBoxModelRanks);
    }
    
    public void refreshTableRank()
    {
        
        jtableRankModel = new DefaultTableModel(new Object[][]{},
                //new Object[]{"ID", "Rango", "P.Vender", "P.Comprar", "P.Productos", "P.Reportes", "P.Empleados", "P.Proveedores", "P.Clientes", "P.Configuracion"}) {
                new Object[]{"ID", "Rango"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        
        for ( Object rank : itemsRank )
        {
            Rank ran = (Rank) rank;
            //jtableRankModel.addRow(new Object[]{ran.getID(), ran.getName(), ran.getPermission().isCan_sells(), ran.getPermission().isCan_buys(), ran.getPermission().isCan_products(), ran.getPermission().isCan_reports(), ran.getPermission().isCan_employees(), ran.getPermission().isCan_providers(), ran.getPermission().isCan_customers(), ran.getPermission().isCan_settings()});
            jtableRankModel.addRow(new Object[]{ran.getID(), ran.getName()});
        }
        
        jTableRanks.setModel(jtableRankModel);
    }
    
    public void setModel()
    {
        List items = new ArrayList();
        
        jTextFieldFirst.setText(this.page + "");
        int counter = 0;
        if (this.page == 1)
            counter = 0;
        else
            counter = ((this.page - 1)*Integer.parseInt(totalEmployee));
        
        for ( int i = 0; i < (this.page*Integer.parseInt(totalEmployee)); i++ )
        {
            if ( i >= counter )
            {
                try
                {
                    items.add(itemsEmployee.get(i));
                }
                catch(Exception e){
                    
                }
            }
        }
                
        model = new EmployeeModel(items);
        jTableEmployee.setModel(model);
        
    }
    
    
    public void buscar()
    {
        List items = new ArrayList();
        for ( Object customer : itemsEmployee)
        {
            Employee cus = (Employee) customer;
            
            if ( cus.getName().toLowerCase().startsWith(jTextField1.getText().toLowerCase()))
                items.add(customer);
            
            model = new EmployeeModel(items);
            jTableEmployee.setModel(model);
        }
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableEmployee = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jTextFieldFirst = new javax.swing.JTextField();
        jLabelDe = new javax.swing.JLabel();
        jTextFieldSecond = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jButtonAddEmployee = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldLastname = new javax.swing.JTextField();
        jTextFieldUser = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldEmail = new javax.swing.JTextField();
        jPasswordField1 = new javax.swing.JPasswordField();
        jLabel6 = new javax.swing.JLabel();
        jComboBoxRank = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableRanks = new javax.swing.JTable();
        jButtonEditRank = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new javax.swing.OverlayLayout(getContentPane()));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jTableEmployee.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableEmployeeMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableEmployee);

        jButton1.setText("<");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTextFieldFirst.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldFirst.setEnabled(false);
        jTextFieldFirst.setFocusable(false);
        jTextFieldFirst.setMinimumSize(new java.awt.Dimension(35, 35));
        jTextFieldFirst.setPreferredSize(new java.awt.Dimension(35, 35));

        jLabelDe.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelDe.setText("de");

        jTextFieldSecond.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldSecond.setEnabled(false);
        jTextFieldSecond.setFocusable(false);
        jTextFieldSecond.setMinimumSize(new java.awt.Dimension(35, 35));
        jTextFieldSecond.setPreferredSize(new java.awt.Dimension(35, 35));

        jButton2.setText(">");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField1KeyTyped(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/search.png"))); // NOI18N
        jLabel1.setText("Buscar:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addGap(5, 5, 5)
                        .addComponent(jTextFieldFirst, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jLabelDe, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jTextFieldSecond, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(jButton2)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldFirst, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabelDe))
                    .addComponent(jTextFieldSecond, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Lista de empleados", new javax.swing.ImageIcon(getClass().getResource("/resources/customerslist.png")), jPanel1); // NOI18N

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jButtonAddEmployee.setText("Agregar");
        jButtonAddEmployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddEmployeeActionPerformed(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Nombre:");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Apellidos:");
        jLabel3.setToolTipText("");

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Usuario:");

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Correo electronico:");

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Contraseña:");

        jComboBoxRank.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione un rango", "Empleado", "Dueño" }));

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Rango:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jTextFieldName, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jTextFieldLastname, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jTextFieldUser, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jTextFieldEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jPasswordField1, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComboBoxRank, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jButtonAddEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 413, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldName, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel3))
                    .addComponent(jTextFieldLastname, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldUser, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPasswordField1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxRank, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addComponent(jButtonAddEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jTabbedPane1.addTab("Agregar empleado", new javax.swing.ImageIcon(getClass().getResource("/resources/customeradd.png")), jPanel2); // NOI18N

        jTableRanks.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Rango"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableRanks.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableRanksMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTableRanks);

        jButtonEditRank.setText("Editar");
        jButtonEditRank.setEnabled(false);
        jButtonEditRank.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditRankActionPerformed(evt);
            }
        });

        jButton3.setText("Agregar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonEditRank, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonEditRank, javax.swing.GroupLayout.DEFAULT_SIZE, 61, Short.MAX_VALUE)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Rangos", new javax.swing.ImageIcon(getClass().getResource("/resources/permissions.png")), jPanel3); // NOI18N

        getContentPane().add(jTabbedPane1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddEmployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddEmployeeActionPerformed
        
        if ( jTextFieldName.getText().isEmpty() || jTextFieldLastname.getText().isEmpty() || jTextFieldUser.getText().isEmpty()
                || jTextFieldEmail.getText().isEmpty() || (new String(jPasswordField1.getPassword())).isEmpty())
            Init.MSG.showMessageError("No deje algún campo sin rellenar");
        else
        {
            if ( this.employeeNow == null)
                this.employeeNow = new Employee(-1);

            this.employeeNow.setName(jTextFieldName.getText());
            this.employeeNow.setLastname(jTextFieldLastname.getText());
            this.employeeNow.setUser(jTextFieldUser.getText());
            this.employeeNow.setEmail(jTextFieldEmail.getText());
            this.employeeNow.setPassword(new String(jPasswordField1.getPassword()));
            this.employeeNow.setRank((Rank) ComboBoxModelRanks.getElementAt(jComboBoxRank.getSelectedIndex()));

            if ( this.employeeNow.save() )
            {
                Init.MSG.showMessageInformation("Empleado registrado/actualizado");
                jTabbedPane1.setSelectedIndex(0);
                loadEmployees();
            }
            else
                Init.MSG.showMessageInformation("El empleado no pudo ser registrado/actualizado");
        }
    }//GEN-LAST:event_jButtonAddEmployeeActionPerformed

    private void jTextField1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyTyped
        buscar();
        buscar();
    }//GEN-LAST:event_jTextField1KeyTyped

    private void jTextField1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyReleased
        buscar();
        buscar();
    }//GEN-LAST:event_jTextField1KeyReleased

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        buscar();
        buscar();
    }//GEN-LAST:event_jTextField1KeyPressed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if ( (this.page+1) <= Integer.parseInt(jTextFieldSecond.getText()) )
        this.page++;
        setModel();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if ( (this.page-1) >= 1 )
        this.page--;
        setModel();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        RankModifyFrame rankmod = new RankModifyFrame();
        rankmod.observable.add(this);
        rankmod.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButtonEditRankActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditRankActionPerformed
        Rank rank = new Rank(Integer.parseInt(jTableRanks.getValueAt(jTableRanks.getSelectedRow(), 0).toString()));
        RankModifyFrame rankmod = new RankModifyFrame(rank);
        rankmod.observable.add(this);
        rankmod.setVisible(true);
    }//GEN-LAST:event_jButtonEditRankActionPerformed

    private void jTableRanksMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableRanksMouseClicked
        this.jButtonEditRank.setEnabled(true);
    }//GEN-LAST:event_jTableRanksMouseClicked

    private void jTableEmployeeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableEmployeeMouseClicked
        if (evt.getClickCount() == 2) {
            this.employeeNow = (Employee) model.getValueAt(jTableEmployee.rowAtPoint(evt.getPoint()), jTableEmployee.columnAtPoint(evt.getPoint()));
            
            Security sec = new Security();
            sec.addKey("BlackBlex");
            jTextFieldName.setText(this.employeeNow.getName());
            jTextFieldLastname.setText(this.employeeNow.getLastname());
            jTextFieldUser.setText(this.employeeNow.getUser());
            jTextFieldEmail.setText(this.employeeNow.getEmail());
            jPasswordField1.setText(sec.desencriptar(this.employeeNow.getPassword()));
            jComboBoxRank.setSelectedItem(this.employeeNow.getRank());
                        
            jTabbedPane1.setSelectedIndex(1);
        }        
    }//GEN-LAST:event_jTableEmployeeMouseClicked

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        MainFrame.setOpenFrame(false);
    }//GEN-LAST:event_formWindowClosing

    @Override
    public void update(Observable obj) {
        loadRanks();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButtonAddEmployee;
    private javax.swing.JButton jButtonEditRank;
    private javax.swing.JComboBox<String> jComboBoxRank;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabelDe;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTableEmployee;
    private javax.swing.JTable jTableRanks;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextFieldEmail;
    private javax.swing.JTextField jTextFieldFirst;
    private javax.swing.JTextField jTextFieldLastname;
    private javax.swing.JTextField jTextFieldName;
    private javax.swing.JTextField jTextFieldSecond;
    private javax.swing.JTextField jTextFieldUser;
    // End of variables declaration//GEN-END:variables

}
