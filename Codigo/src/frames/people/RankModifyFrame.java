package frames.people;

import bin.Main;
import com.blackblex.libs.application.components.styles.JButtonStyleFlat;
import com.blackblex.libs.main.Init;
import com.blackblex.libs.system.utils.Observable;
import utils.objects.Rank;

public class RankModifyFrame extends javax.swing.JFrame {

    public Observable observable = new Observable() {
        @Override
        public void action() {
            
        }
    };
    
    private Rank rank = null;
    
    public RankModifyFrame() {
        initComponents();
        
        init();
    }
    
    public RankModifyFrame(Rank rank) {
        initComponents();
        
        this.rank = rank;
        this.rank.get();
        
        this.jTextField1.setText(Integer.toString(this.rank.getID()));
        this.jTextField2.setText(this.rank.getName());
        
        this.jCheckBoxSells.setSelected(this.rank.getPermission().isCan_sells());
        this.jCheckBoxBuys.setSelected(this.rank.getPermission().isCan_buys());
        this.jCheckBoxProducts.setSelected(this.rank.getPermission().isCan_products());
        this.jCheckBoxProviders.setSelected(this.rank.getPermission().isCan_providers());
        this.jCheckBoxSettings.setSelected(this.rank.getPermission().isCan_settings());
        this.jCheckBoxCustomers.setSelected(this.rank.getPermission().isCan_customers());
        this.jCheckBoxEmployees.setSelected(this.rank.getPermission().isCan_employees());
        this.jCheckBoxReports.setSelected(this.rank.getPermission().isCan_reports());
        this.jCheckBoxTrademarks.setSelected(this.rank.getPermission().isCan_trademarks());
        this.jCheckBoxCategories.setSelected(this.rank.getPermission().isCan_categories());
        
        this.jButton1.setText("Actualizar");
        this.jButton2.setEnabled(true);
        
        init();
    }

    private void init()
    {
        
        JButtonStyleFlat jbuttonadd = new JButtonStyleFlat(jButton1, Main.buttonColorAccept, 3);
        jButton1.setUI(jbuttonadd);

        JButtonStyleFlat jbuttondelete = new JButtonStyleFlat(jButton2, Main.buttonColorDecline, 3);
        jButton2.setUI(jbuttondelete);
        
        this.setLocationRelativeTo(null);
        this.setTitle(Main.nameApp + " ~ Rangos");
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jCheckBoxSells = new javax.swing.JCheckBox();
        jCheckBoxBuys = new javax.swing.JCheckBox();
        jCheckBoxProducts = new javax.swing.JCheckBox();
        jCheckBoxReports = new javax.swing.JCheckBox();
        jCheckBoxEmployees = new javax.swing.JCheckBox();
        jCheckBoxProviders = new javax.swing.JCheckBox();
        jCheckBoxCustomers = new javax.swing.JCheckBox();
        jCheckBoxSettings = new javax.swing.JCheckBox();
        jCheckBoxTrademarks = new javax.swing.JCheckBox();
        jCheckBoxCategories = new javax.swing.JCheckBox();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new javax.swing.OverlayLayout(getContentPane()));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Permisos [Modulos que puede ver el rango]", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 0, 12))); // NOI18N

        jCheckBoxSells.setText("Ventas");
        jCheckBoxSells.setOpaque(false);

        jCheckBoxBuys.setText("Compras");
        jCheckBoxBuys.setOpaque(false);

        jCheckBoxProducts.setText("Productos");
        jCheckBoxProducts.setOpaque(false);

        jCheckBoxReports.setText("Reportes");
        jCheckBoxReports.setOpaque(false);

        jCheckBoxEmployees.setText("Empleados");
        jCheckBoxEmployees.setOpaque(false);

        jCheckBoxProviders.setText("Proveedores");
        jCheckBoxProviders.setOpaque(false);

        jCheckBoxCustomers.setText("Clientes");
        jCheckBoxCustomers.setOpaque(false);

        jCheckBoxSettings.setText("Configuración");
        jCheckBoxSettings.setOpaque(false);

        jCheckBoxTrademarks.setText("Marcas");
        jCheckBoxTrademarks.setOpaque(false);

        jCheckBoxCategories.setText("Categorias");
        jCheckBoxCategories.setOpaque(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jCheckBoxSells, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCheckBoxBuys, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCheckBoxProducts, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jCheckBoxCustomers, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jCheckBoxSettings, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jCheckBoxReports, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jCheckBoxEmployees, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCheckBoxProviders, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCheckBoxTrademarks, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jCheckBoxCategories))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxSells)
                    .addComponent(jCheckBoxBuys)
                    .addComponent(jCheckBoxProducts))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxReports)
                    .addComponent(jCheckBoxEmployees)
                    .addComponent(jCheckBoxProviders))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxCustomers)
                    .addComponent(jCheckBoxSettings)
                    .addComponent(jCheckBoxTrademarks))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBoxCategories))
        );

        jButton1.setText("Añadir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("ID:");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Nombre:");

        jTextField1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField1.setEnabled(false);

        jTextField2.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jButton2.setText("Eliminar");
        jButton2.setEnabled(false);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField1)
                            .addComponent(jTextField2)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 56, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(12, 12, 12))
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        
        if ( jTextField2.getText().isEmpty())
            Init.MSG.showMessageError("No deje algún campo sin rellenar");
        else
        {
            if ( this.rank == null )
                this.rank = new Rank(-1);

            this.rank.setName(this.jTextField2.getText());
            this.rank.getPermission().setCan_sells(this.jCheckBoxSells.isSelected());
            this.rank.getPermission().setCan_buys(this.jCheckBoxBuys.isSelected());
            this.rank.getPermission().setCan_products(this.jCheckBoxProducts.isSelected());
            this.rank.getPermission().setCan_providers(this.jCheckBoxProviders.isSelected());
            this.rank.getPermission().setCan_settings(this.jCheckBoxSettings.isSelected());
            this.rank.getPermission().setCan_customers(this.jCheckBoxCustomers.isSelected());
            this.rank.getPermission().setCan_employees(this.jCheckBoxEmployees.isSelected());
            this.rank.getPermission().setCan_reports(this.jCheckBoxReports.isSelected());
            this.rank.getPermission().setCan_trademarks(this.jCheckBoxTrademarks.isSelected());
            this.rank.getPermission().setCan_categories(this.jCheckBoxCategories.isSelected());

            this.rank.save();
            Init.MSG.showMessageInformation("Rango actualizado/añadido correctamente.");

            this.observable.notifyObs();        
            this.dispose();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if ( this.rank.delete() )
            Init.MSG.showMessageInformation("Rango eliminado correctamente");
        else
            Init.MSG.showMessageError("El rango no fue eliminado");
        
        this.observable.notifyObs();        
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JCheckBox jCheckBoxBuys;
    private javax.swing.JCheckBox jCheckBoxCategories;
    private javax.swing.JCheckBox jCheckBoxCustomers;
    private javax.swing.JCheckBox jCheckBoxEmployees;
    private javax.swing.JCheckBox jCheckBoxProducts;
    private javax.swing.JCheckBox jCheckBoxProviders;
    private javax.swing.JCheckBox jCheckBoxReports;
    private javax.swing.JCheckBox jCheckBoxSells;
    private javax.swing.JCheckBox jCheckBoxSettings;
    private javax.swing.JCheckBox jCheckBoxTrademarks;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}
