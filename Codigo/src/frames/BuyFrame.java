package frames;

import frames.products.SearchProductFrame;
import bin.Main;
import com.blackblex.libs.application.components.borders.RoundedSidesBorder;
import com.blackblex.libs.application.components.styles.JButtonStyleFlat;
import com.blackblex.libs.application.components.styles.JComboBoxStyleMetro;
import com.blackblex.libs.main.Init;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.Observer;
import com.blackblex.libs.system.utils.MultiString;
import com.blackblex.libs.system.utils.Observable;
import frames.actions.MainFrame;
import java.awt.Color;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import utils.objects.BuyTransaction;
import utils.objects.Customer;
import utils.objects.Products;
import utils.objects.Provider;
import utils.objects.Ticket;
import utils.objects.SellTransaction;

public class BuyFrame extends javax.swing.JFrame implements Observer {

    DefaultComboBoxModel ComboBoxModelProviders;
    DefaultTableModel jtableBuyModel;
    
    public Observable observable = new Observable() {
        @Override
        public void action() {
            
        }
    };

    private double operationTotal, operationSubTotal, operationDiscount =0;

    public BuyFrame() {
        initComponents();

        JButtonStyleFlat jbuttonbuy = new JButtonStyleFlat(jButton2, Main.buttonColorAccept, 3);
        jButton2.setUI(jbuttonbuy);

        JButtonStyleFlat jbuttoncancel = new JButtonStyleFlat(jButton3, Main.buttonColorDecline, 3);
        jButton3.setUI(jbuttoncancel);

        jLabelTotal.setBackground(Color.decode(Main.buttonColorAccept));
        jLabelTotal.setBorder(new RoundedSidesBorder(Color.decode(Main.buttonColorAccept), 3, 15, 15));

        jTextFieldID.setText(Init.getRandomCode());
        jTextFieldDate.setText(Init.getDate());

        jTextFieldID.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
        jTextFieldDate.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));

        JButtonStyleFlat jbuttonsearchproduct = new JButtonStyleFlat(this.jButtonSearchProduct, Main.buttonColorSearch, 0);
        this.jButtonSearchProduct.setUI(jbuttonsearchproduct);

        jComboBoxProviders.setUI(JComboBoxStyleMetro.createUI(jComboBoxProviders, Color.decode(Main.comboboxColorPeople)));

        loadProviders();

        jtableBuyModel = new DefaultTableModel(new Object[][]{},
                new Object[]{"Codigo", "Descripción", "Precio unitario", "Cantidad", "Descuento", "Total"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };

        jTableProducts.setModel(jtableBuyModel);

        TableCellRenderer rendererFromHeader = jTableProducts.getTableHeader().getDefaultRenderer();
        JLabel headerLabel = (JLabel) rendererFromHeader;
        headerLabel.setHorizontalAlignment(JLabel.CENTER);

        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
        jTableProducts.getColumnModel().getColumn(0).setCellRenderer(dtcr);
        jTableProducts.getColumnModel().getColumn(1).setCellRenderer(dtcr);
        jTableProducts.getColumnModel().getColumn(2).setCellRenderer(dtcr);
        jTableProducts.getColumnModel().getColumn(3).setCellRenderer(dtcr);
        jTableProducts.getColumnModel().getColumn(4).setCellRenderer(dtcr);
        jTableProducts.getColumnModel().getColumn(5).setCellRenderer(dtcr);
        jTableProducts.setAutoCreateRowSorter(true);
        jTableProducts.getTableHeader().setReorderingAllowed(false);
        jTableProducts.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTableProducts.setRowHeight(35);

        this.setLocationRelativeTo(null);
        this.setTitle(Main.nameApp + " ~ Comprar producto");
    }

    public void loadProviders() {
        ComboBoxModelProviders = new DefaultComboBoxModel();

        OperationSQL sql = Main.SQL;

        sql.rows.put("id", "");
        MultiString[] result = sql.select("providers");
        if ( result != null )
        {
            for (MultiString r : result) {
                Provider i = new Provider(Integer.parseInt(r.getOne()));
                i.get();
                ComboBoxModelProviders.addElement(i);
            }

            jComboBoxProviders.setModel(ComboBoxModelProviders);
        }
    }

    @Override
    public void update(Observable obj) {
        Products prod = (Products) obj.getArgs(0);

        int units = Init.MSG.getInt("¿Cuantas unidades va a comprar?");

        if ( units > 0 )
        {
            double total = prod.getPrice() * (double) units;
            double discount = (total * (prod.getCategory().getDiscount() / 100.00));

            jtableBuyModel.addRow(new Object[]{prod.getID(), prod.getDescription(),
                "$" + String.format("%.2f", prod.getPrice()), units, "%" + String.format("%.2f", prod.getCategory().getDiscount()), "$" + String.format("%.2f", total)});

            this.operationSubTotal += total;
            //this.operationDiscount += discount;
            this.operationTotal += (total - discount);

            setTotal();
        }
    }

    private void removeProducts() {
        if (jtableBuyModel.getRowCount() != 0) {
            
            OperationSQL sql = Main.SQL;
            Date date = sql.getDate(); Time time = sql.getTime();
            ArrayList<ArrayList<String>> items = new ArrayList<>();
            Provider provider = (Provider) ComboBoxModelProviders.getElementAt(jComboBoxProviders.getSelectedIndex());
            provider.get();
            
            if (jtableBuyModel.getRowCount() > 0) {
                int i;
                
                BuyTransaction buytransaction = new BuyTransaction(jTextFieldID.getText());
                buytransaction.setDate(date);
                buytransaction.setTime(time);
                buytransaction.setTotal(this.operationTotal);
                buytransaction.setId_provider(provider.getID());
                
                for (i = jtableBuyModel.getRowCount() - 1; i >= 0; i--) {
                    Products prod = new Products((String) jtableBuyModel.getValueAt(i, 0));
                    prod.get();
                    prod.setStock(prod.getStock() + Integer.parseInt(jtableBuyModel.getValueAt(i, 3).toString()));
                    prod.save();

                    buytransaction.setBuyDetail(Integer.parseInt(jtableBuyModel.getValueAt(i, 3).toString()), prod);
                    
                    ArrayList<String> temp = new ArrayList<>();
                    temp.add(prod.getDescription() + " - " + prod.getPresentation());
                    temp.add(jtableBuyModel.getValueAt(i, 3).toString());
                    temp.add(Double.toString(prod.getPrice()));
                    temp.add(((String)jtableBuyModel.getValueAt(i, 5)).replace("$", ""));
                    items.add(temp);

                    jtableBuyModel.removeRow(i);
                }
                
                buytransaction.save();
            }
            
            String providerName = provider.getName();
            Init.MSG.showMessageInformation("Compra exitosa!");
            Ticket ticket = new Ticket(1, Main.nameApp, Main.street2, Main.street, jTextFieldID.getText(),
                    date.toString(), time.toString(), Main.employee.getUser(), providerName, Double.toString(this.operationSubTotal), Double.toString(this.operationDiscount), Double.toString(this.operationTotal), "0", "0",
                    items);

            ticket.process();
            
            this.observable.notifyObs();
            this.dispose();
        } else {
            Init.MSG.showMessageError("Necesita por lo menos un producto para comprar.");
        }
    }

    private void setTotal()
    {
        jLabelTotal.setText("<html><table>"
                + "<tr>" 
                + "<td align=\"right\" style=\"width: 50px;\"><strong>Subtotal: </strong></td>"
                + "<td align=\"right\">$" + String.format("%.2f", this.operationSubTotal) + "</td>"
                + "</tr>"
                + "<tr>" 
                + "<td align=\"right\" style=\"width: 50px;\"><strong>Descuento: </strong></td>"
                + "<td align=\"right\">-$" + String.format("%.2f", this.operationDiscount) + "</td>"
                + "</tr>"
                + "<tr>" 
                + "<td align=\"right\" style=\"width: 50px;\"><strong>Total: </strong></td>"
                + "<td align=\"right\">$" + String.format("%.2f", this.operationTotal) + "</td>"
                + "</tr>"
                + "</table>"
                + "</html>");
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableProducts = new javax.swing.JTable();
        jLabelTotal = new javax.swing.JLabel();
        jLabelIDSell = new javax.swing.JLabel();
        jTextFieldID = new javax.swing.JTextField();
        jTextFieldDate = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jComboBoxProviders = new javax.swing.JComboBox<>();
        jButtonSearchProduct = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new javax.swing.OverlayLayout(getContentPane()));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jTableProducts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Descripción", "Precio unitario", "Cantidad", "Descuento", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableProducts.setToolTipText("Doble click para remover el producto.");
        jTableProducts.setColumnSelectionAllowed(true);
        jTableProducts.getTableHeader().setReorderingAllowed(false);
        jTableProducts.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableProductsMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableProducts);
        jTableProducts.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (jTableProducts.getColumnModel().getColumnCount() > 0) {
            jTableProducts.getColumnModel().getColumn(0).setResizable(false);
            jTableProducts.getColumnModel().getColumn(1).setResizable(false);
            jTableProducts.getColumnModel().getColumn(2).setResizable(false);
            jTableProducts.getColumnModel().getColumn(3).setResizable(false);
            jTableProducts.getColumnModel().getColumn(4).setResizable(false);
            jTableProducts.getColumnModel().getColumn(5).setResizable(false);
        }

        jLabelTotal.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabelTotal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelTotal.setOpaque(true);

        jLabelIDSell.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelIDSell.setText("Venta:");

        jTextFieldID.setEditable(false);

        jTextFieldDate.setEditable(false);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Fecha:");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Proveedor:");

        jButtonSearchProduct.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/search white.png"))); // NOI18N
        jButtonSearchProduct.setText("Buscar producto");
        jButtonSearchProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchProductActionPerformed(evt);
            }
        });

        jButton2.setText("Comprar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Cancelar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelIDSell, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextFieldID, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextFieldDate, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBoxProviders, javax.swing.GroupLayout.PREFERRED_SIZE, 401, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonSearchProduct, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelIDSell, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jTextFieldID, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldDate, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxProviders, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSearchProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSearchProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchProductActionPerformed
        SearchProductFrame search = new SearchProductFrame((Provider) ComboBoxModelProviders.getElementAt(jComboBoxProviders.getSelectedIndex()));
        search.observable.add(this);
        search.setVisible(true);
    }//GEN-LAST:event_jButtonSearchProductActionPerformed

    private void jTableProductsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableProductsMouseClicked
        if (evt.getClickCount() == 2) {
            Products prod = new Products(jTableProducts.getValueAt(jTableProducts.getSelectedRow(), 0).toString());
            prod.get();
            int units = Integer.parseInt(jTableProducts.getValueAt(jTableProducts.getSelectedRow(), 3).toString());
            
            double total = prod.getPrice()* (double) units;
            double discount = (total*(prod.getCategory().getDiscount()/100.00));
            this.operationSubTotal -= total;
            this.operationDiscount -= discount;
            this.operationTotal -= (total - discount);
            setTotal();
            jtableBuyModel.removeRow(jTableProducts.getSelectedRow());
        }       
    }//GEN-LAST:event_jTableProductsMouseClicked

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        removeProducts();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        MainFrame.setOpenFrame(false);
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButtonSearchProduct;
    private javax.swing.JComboBox<String> jComboBoxProviders;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabelIDSell;
    private javax.swing.JLabel jLabelTotal;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableProducts;
    private javax.swing.JTextField jTextFieldDate;
    private javax.swing.JTextField jTextFieldID;
    // End of variables declaration//GEN-END:variables

}
