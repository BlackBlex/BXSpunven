package frames.actions;

import bin.Main;
import com.blackblex.libs.application.components.customs.TextPrompt;
import com.blackblex.libs.application.components.borders.RoundedSidesBorder;
import com.blackblex.libs.application.components.styles.JButtonStyleFlat;
import com.blackblex.libs.main.Init;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;
import com.blackblex.libs.system.utils.Security;
import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import utils.objects.Employee;

public class LoginFrame extends javax.swing.JFrame {

    private TextPrompt textUser, textPass;
    
    public LoginFrame() {
        initComponents();

        JButtonStyleFlat jbutton1 = new JButtonStyleFlat(jButtonConnect, Main.buttonColorSecundary, 0);
        jButtonConnect.setUI(jbutton1);

        textUser = new TextPrompt("Usuario", jTextFieldUsername);
        textPass = new TextPrompt("Contraseña", jPasswordField1);
        
        textUser.setForeground(Color.decode("#2c3e50"));
        textPass.setForeground(Color.decode("#2c3e50"));
        
        jTextFieldUsername.setBorder(BorderFactory.createCompoundBorder( new RoundedSidesBorder(Color.darkGray, 2, 0, 0), BorderFactory.createEmptyBorder(5, 10, 5, 10)));
        jPasswordField1.setBorder(BorderFactory.createCompoundBorder( new RoundedSidesBorder(Color.darkGray, 2, 0, 0), BorderFactory.createEmptyBorder(5, 10, 5, 10)));
        
        jLabel1.setFont(new Font(getFont().getName(), Font.BOLD, 30));
        jTextFieldUsername.setFont(new Font(getFont().getName(), Font.BOLD, 15));
        jPasswordField1.setFont(new Font(getFont().getName(), Font.BOLD, 15));

        //jButton1.setFont(new Font(getFont().getName(), Font.BOLD, 20));
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jButtonConnect = new javax.swing.JButton();
        jTextFieldUsername = new javax.swing.JTextField();
        jPasswordField1 = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Logueo");
        setResizable(false);
        getContentPane().setLayout(new javax.swing.OverlayLayout(getContentPane()));

        jPanel1.setBackground(new java.awt.Color(35, 99, 142));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setPreferredSize(new java.awt.Dimension(350, 250));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        jLabel1.setBackground(new java.awt.Color(109, 111, 110));
        jLabel1.setForeground(new java.awt.Color(145, 145, 145));
        jLabel1.setText("Conectarse");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 5, 0);
        jPanel2.add(jLabel1, gridBagConstraints);

        jButtonConnect.setText("Conectarse");
        jButtonConnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonConnectActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipady = 23;
        jPanel2.add(jButtonConnect, gridBagConstraints);

        jTextFieldUsername.setForeground(new java.awt.Color(102, 102, 102));
        jTextFieldUsername.setPreferredSize(new java.awt.Dimension(200, 40));
        jTextFieldUsername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldUsernameActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(6, 0, 6, 0);
        jPanel2.add(jTextFieldUsername, gridBagConstraints);

        jPasswordField1.setForeground(new java.awt.Color(102, 102, 102));
        jPasswordField1.setPreferredSize(new java.awt.Dimension(200, 40));
        jPasswordField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jPasswordField1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 6, 0);
        jPanel2.add(jPasswordField1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(20, 20, 20, 20);
        jPanel1.add(jPanel2, gridBagConstraints);

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonConnectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonConnectActionPerformed

        connect();

    }//GEN-LAST:event_jButtonConnectActionPerformed

    private void jTextFieldUsernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldUsernameActionPerformed
        jPasswordField1.requestFocus();
    }//GEN-LAST:event_jTextFieldUsernameActionPerformed

    private void jPasswordField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jPasswordField1ActionPerformed
        connect();
    }//GEN-LAST:event_jPasswordField1ActionPerformed

    public void connect() {
        OperationSQL sql = Main.SQL;
        Security sec = new Security();
        sec.addKey("BlackBlex");
        String username = jTextFieldUsername.getText().replaceAll("'", "");
        String password = new String(jPasswordField1.getPassword());
        password = sec.encriptar(password);
        
        sql.rows.put("id", "name");
        sql.rows.put("lastname", "email");
        sql.rows.put("pass", "rank");
        sql.wheres.put(new MultiString("user", username), new MultiString("=", ""));
        MultiString[] result = sql.select("employees");

        if (result != null) {
            for (MultiString user : result) {
                if (password.equals(user.getFive())) {
                    
                    MainFrame.openFrame = false;
                    Main.employee = new Employee(Integer.parseInt(user.getOne()));
                    Main.employee.get();
                    new MainFrame().setVisible(true);
                    this.dispose();
                } else {
                    Init.MSG.showMessageError("Contraseña incorrecta");
                }
            }
        } else {
            Init.MSG.showMessageError("Usuario incorrecto");
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonConnect;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JTextField jTextFieldUsername;
    // End of variables declaration//GEN-END:variables
}
