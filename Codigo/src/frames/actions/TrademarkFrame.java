package frames.actions;

import bin.Main;
import com.blackblex.libs.application.components.styles.JButtonStyleFlat;
import com.blackblex.libs.main.Init;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import utils.objects.Category;
import utils.objects.Products;
import utils.objects.Provider;
import utils.objects.Trademark;

public class TrademarkFrame extends javax.swing.JFrame {

    private OperationSQL sql = Main.SQL;
    private int categoryTradeID, providerTradeID;
    DefaultComboBoxModel ComboBoxModelCategories;
    DefaultComboBoxModel ComboBoxModelProducts;
    DefaultComboBoxModel ComboBoxModelProviders;

    DefaultComboBoxModel ComboBoxModelTrademarks;
    DefaultComboBoxModel ComboBoxModelTrademarksPro;
    DefaultComboBoxModel ComboBoxModelTrademarksProv;
    
    DefaultListModel listModelTrademarks;
    DefaultListModel listModelProducts;
    DefaultListModel listModelCategories;

    public TrademarkFrame() {
        initComponents();

        JButtonStyleFlat jbuttonaddtrademark = new JButtonStyleFlat(jButtonAddTrademark, Main.buttonColorAccept2, 0);
        jButtonAddTrademark.setUI(jbuttonaddtrademark);


        JButtonStyleFlat jbuttondeletetrademark = new JButtonStyleFlat(jButtonDeleteTrademark, Main.buttonColorDecline2, 0);
        jButtonDeleteTrademark.setUI(jbuttondeletetrademark);
        JButtonStyleFlat jbuttontrademarkrefresh = new JButtonStyleFlat(jButtonTrademarkRefresh, "#F76363", 50);
        jButtonTrademarkRefresh.setUI(jbuttontrademarkrefresh);
        JButtonStyleFlat jbuttonsavecattra = new JButtonStyleFlat(jButtonSaveCatTra, Main.buttonColorSave, 0);
        jButtonSaveCatTra.setUI(jbuttonsavecattra);
        JButtonStyleFlat jbuttonsaveprotra = new JButtonStyleFlat(jButtonSaveProTra, Main.buttonColorSave, 0);
        jButtonSaveProTra.setUI(jbuttonsaveprotra);
        JButtonStyleFlat jbuttonsaveprovtra = new JButtonStyleFlat(jButtonSaveProvTra, Main.buttonColorSave, 0);
        jButtonSaveProvTra.setUI(jbuttonsaveprovtra);


        jButtonDeleteTrademark.setEnabled(false);
        //jButtonSaveProvTra.setEnabled(false);

        loadTrademarks();
        loadCategories();
        loadProducts();
        loadProviders();

        this.setLocationRelativeTo(null);
        this.setTitle(Main.nameApp + " ~ Marcas");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelTradeMarks = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldCodeTrademark = new javax.swing.JTextField();
        jTextFieldNameTrademark = new javax.swing.JTextField();
        jButtonAddTrademark = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListTrademark = new javax.swing.JList<>();
        jButtonDeleteTrademark = new javax.swing.JButton();
        jButtonTrademarkRefresh = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListProductsTrademark = new javax.swing.JList<>();
        jPanel4 = new javax.swing.JPanel();
        jComboBoxCategories = new javax.swing.JComboBox<>();
        jComboBoxTrademarkCat = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jCheckBoxCategory = new javax.swing.JCheckBox();
        jButtonSaveCatTra = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jComboBoxProducts = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jComboBoxTrademarkPro = new javax.swing.JComboBox<>();
        jCheckBoxProducts = new javax.swing.JCheckBox();
        jButtonSaveProTra = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jComboBoxProviders = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jComboBoxTrademarkProv = new javax.swing.JComboBox<>();
        jCheckBoxProviders = new javax.swing.JCheckBox();
        jButtonSaveProvTra = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jListCategoriesTrademark = new javax.swing.JList<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new javax.swing.OverlayLayout(getContentPane()));

        jPanelTradeMarks.setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Agregar marca", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jPanel1.setOpaque(false);

        jLabel1.setText("Nombre de la marca:");

        jLabel2.setText("Codigo de la marca:");

        jButtonAddTrademark.setText("Agregar");
        jButtonAddTrademark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddTrademarkActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldCodeTrademark, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextFieldNameTrademark)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 235, Short.MAX_VALUE)
                        .addComponent(jButtonAddTrademark, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldNameTrademark, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldCodeTrademark, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonAddTrademark, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Marcas"));
        jPanel2.setOpaque(false);

        jListTrademark.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jListTrademark.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListTrademarkValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jListTrademark);

        jButtonDeleteTrademark.setText("Eliminar");
        jButtonDeleteTrademark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteTrademarkActionPerformed(evt);
            }
        });

        jButtonTrademarkRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/settings-refresh.png"))); // NOI18N
        jButtonTrademarkRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTrademarkRefreshActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButtonDeleteTrademark, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonTrademarkRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButtonDeleteTrademark, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonTrademarkRefresh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Productos de la marca"));
        jPanel3.setOpaque(false);
        jPanel3.setPreferredSize(new java.awt.Dimension(192, 260));

        jScrollPane2.setViewportView(jListProductsTrademark);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Categorias & marcas", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jPanel4.setOpaque(false);
        jPanel4.setPreferredSize(new java.awt.Dimension(220, 313));

        jComboBoxCategories.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxCategoriesItemStateChanged(evt);
            }
        });

        jComboBoxTrademarkCat.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxTrademarkCatItemStateChanged(evt);
            }
        });

        jLabel3.setText("Categoria:");

        jLabel4.setText("Marca:");

        jCheckBoxCategory.setText("Enlazar");
        jCheckBoxCategory.setOpaque(false);

        jButtonSaveCatTra.setText("Guardar");
        jButtonSaveCatTra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveCatTraActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jComboBoxTrademarkCat, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonSaveCatTra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jCheckBoxCategory, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBoxCategories, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(18, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(jComboBoxCategories, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(jComboBoxTrademarkCat, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jCheckBoxCategory)
                .addGap(18, 18, 18)
                .addComponent(jButtonSaveCatTra, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Productos & marcas", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jPanel5.setOpaque(false);
        jPanel5.setPreferredSize(new java.awt.Dimension(220, 313));

        jLabel5.setText("Producto:");

        jComboBoxProducts.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxProductsItemStateChanged(evt);
            }
        });

        jLabel6.setText("Marca:");

        jComboBoxTrademarkPro.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxTrademarkProItemStateChanged(evt);
            }
        });

        jCheckBoxProducts.setText("Enlazar");
        jCheckBoxProducts.setOpaque(false);

        jButtonSaveProTra.setText("Guardar");
        jButtonSaveProTra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveProTraActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBoxTrademarkPro, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBoxProducts, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBoxProducts, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSaveProTra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(18, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addComponent(jComboBoxProducts, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addComponent(jComboBoxTrademarkPro, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jCheckBoxProducts)
                .addGap(18, 18, 18)
                .addComponent(jButtonSaveProTra, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Proveedores & marcas", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jPanel6.setOpaque(false);

        jLabel7.setText("Proveedor:");

        jComboBoxProviders.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxProvidersItemStateChanged(evt);
            }
        });

        jLabel8.setText("Marca:");

        jComboBoxTrademarkProv.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxTrademarkProvItemStateChanged(evt);
            }
        });

        jCheckBoxProviders.setText("Enlazar");
        jCheckBoxProviders.setOpaque(false);

        jButtonSaveProvTra.setText("Guardar");
        jButtonSaveProvTra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSaveProvTraActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jComboBoxTrademarkProv, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxProviders, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jCheckBoxProviders, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonSaveProvTra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap(18, Short.MAX_VALUE)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(jComboBoxProviders, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(jComboBoxTrademarkProv, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jCheckBoxProviders)
                .addGap(18, 18, 18)
                .addComponent(jButtonSaveProvTra, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder("Categorias de la marca"));
        jPanel7.setOpaque(false);

        jScrollPane3.setViewportView(jListCategoriesTrademark);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3)
        );

        javax.swing.GroupLayout jPanelTradeMarksLayout = new javax.swing.GroupLayout(jPanelTradeMarks);
        jPanelTradeMarks.setLayout(jPanelTradeMarksLayout);
        jPanelTradeMarksLayout.setHorizontalGroup(
            jPanelTradeMarksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelTradeMarksLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelTradeMarksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanelTradeMarksLayout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelTradeMarksLayout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addGroup(jPanelTradeMarksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelTradeMarksLayout.setVerticalGroup(
            jPanelTradeMarksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTradeMarksLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelTradeMarksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelTradeMarksLayout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanelTradeMarksLayout.createSequentialGroup()
                        .addGroup(jPanelTradeMarksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                        .addGroup(jPanelTradeMarksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(17, 17, 17)))
                .addContainerGap())
        );

        getContentPane().add(jPanelTradeMarks);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddTrademarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddTrademarkActionPerformed
        addTrademark();
    }//GEN-LAST:event_jButtonAddTrademarkActionPerformed

    private void jListTrademarkValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListTrademarkValueChanged
        try
        {
            jButtonDeleteTrademark.setEnabled(true);
            Trademark trad = (Trademark) listModelTrademarks.getElementAt(jListTrademark.getSelectedIndex());
            //System.out.println(trad.getID() + " " + trad.getCode() + " " + trad.getName());
            loadCategories(trad.getID());
            loadProducts(trad.getID());
        }
        catch ( ArrayIndexOutOfBoundsException e)
        {

        }
    }//GEN-LAST:event_jListTrademarkValueChanged

    private void jButtonDeleteTrademarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteTrademarkActionPerformed
        Trademark trad = (Trademark) listModelTrademarks.getElementAt(jListTrademark.getSelectedIndex());
        if ( trad.delete() )
            Init.MSG.showMessageInformation("Marca eliminada correctamente");
        else
            Init.MSG.showMessageInformation("La marca no se pudo eliminar");
    }//GEN-LAST:event_jButtonDeleteTrademarkActionPerformed

    private void jButtonTrademarkRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTrademarkRefreshActionPerformed
        loadTrademarks();
    }//GEN-LAST:event_jButtonTrademarkRefreshActionPerformed

    private void jComboBoxCategoriesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxCategoriesItemStateChanged
        changeCatTra();
    }//GEN-LAST:event_jComboBoxCategoriesItemStateChanged

    private void jComboBoxTrademarkCatItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxTrademarkCatItemStateChanged
        changeCatTra();
    }//GEN-LAST:event_jComboBoxTrademarkCatItemStateChanged

    private void jButtonSaveCatTraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveCatTraActionPerformed
        saveCatTra();
    }//GEN-LAST:event_jButtonSaveCatTraActionPerformed

    private void jComboBoxProductsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxProductsItemStateChanged
        changeProTra();
    }//GEN-LAST:event_jComboBoxProductsItemStateChanged

    private void jComboBoxTrademarkProItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxTrademarkProItemStateChanged
        changeProTra();
    }//GEN-LAST:event_jComboBoxTrademarkProItemStateChanged

    private void jButtonSaveProTraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveProTraActionPerformed
        saveProTra();
    }//GEN-LAST:event_jButtonSaveProTraActionPerformed

    private void jButtonSaveProvTraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSaveProvTraActionPerformed
        saveProvTra();
    }//GEN-LAST:event_jButtonSaveProvTraActionPerformed

    private void jComboBoxProvidersItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxProvidersItemStateChanged
        changeProvTra();
    }//GEN-LAST:event_jComboBoxProvidersItemStateChanged

    private void jComboBoxTrademarkProvItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxTrademarkProvItemStateChanged
        changeProvTra();
    }//GEN-LAST:event_jComboBoxTrademarkProvItemStateChanged

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        MainFrame.setOpenFrame(false);
    }//GEN-LAST:event_formWindowClosing

    
    public void loadCategories()
    {
        ComboBoxModelCategories = new DefaultComboBoxModel();
        ComboBoxModelCategories.addElement("Seleccione una categoria");
        sql.rows.put("id", "");
        MultiString[] categ = sql.select("categories");
        if ( categ != null )
        {
            for (MultiString c : categ) {
                Category cat = new Category(Integer.parseInt(c.getOne()));
                cat.get();
                ComboBoxModelCategories.addElement(cat);
            }
            jComboBoxCategories.setModel(ComboBoxModelCategories);
        }
    }

    public void loadProducts()
    {
        ComboBoxModelProducts = new DefaultComboBoxModel();
        ComboBoxModelProducts.addElement("Seleccione un producto");

        sql.rows.put("id", "");
        MultiString[] prod = sql.select("products");
        if ( prod != null )
        {
            for (MultiString p : prod) {
                Products pro = new Products(p.getOne());
                pro.get();
                ComboBoxModelProducts.addElement(pro);
            }
            jComboBoxProducts.setModel(ComboBoxModelProducts);
        }
    }

    public void loadProviders()
    {
        ComboBoxModelProviders = new DefaultComboBoxModel();
        ComboBoxModelProviders.addElement("Seleccione un proveedor");
        
        sql.rows.put("id", "");
        MultiString[] prov = sql.select("providers");
        if ( prov != null )
        {
            for (MultiString p : prov) {
                Provider pro = new Provider(Integer.parseInt(p.getOne()));
                pro.get();
                ComboBoxModelProviders.addElement(pro);
            }
            jComboBoxProviders.setModel(ComboBoxModelProviders);
        }
    }

    public void loadTrademarks()
    {
        listModelTrademarks = new DefaultListModel();
        ComboBoxModelTrademarks = new DefaultComboBoxModel();
        ComboBoxModelTrademarksPro = new DefaultComboBoxModel();
        ComboBoxModelTrademarksProv = new DefaultComboBoxModel();
        ComboBoxModelTrademarks.addElement("Sin marca");
        ComboBoxModelTrademarksPro.addElement("Sin marca");
        ComboBoxModelTrademarksProv.addElement("Sin marca");
        sql.rows.put("id", "name");
        MultiString[] tra = sql.select("trademark");
        if ( tra != null )
        {
            for (MultiString t : tra) {
                Trademark trad = new Trademark(Integer.parseInt(t.getOne()));
                trad.get();

                ComboBoxModelTrademarks.addElement(trad);
                ComboBoxModelTrademarksPro.addElement(trad);
                ComboBoxModelTrademarksProv.addElement(trad);
                listModelTrademarks.addElement(trad);
            }
            jComboBoxTrademarkCat.setModel(ComboBoxModelTrademarks);
            jComboBoxTrademarkPro.setModel(ComboBoxModelTrademarksPro);
            jComboBoxTrademarkProv.setModel(ComboBoxModelTrademarksProv);
            jListTrademark.setModel(listModelTrademarks);
        }
    }
    

    public void loadProducts(int trademark)
    {
        listModelProducts = new DefaultListModel();
        
        sql.rows.put("id", "");
        sql.wheres.put(new MultiString("id_trademark", Integer.toString(trademark)), new MultiString("=", ""));
        MultiString[] prod = sql.select("products");
        if ( prod != null )
        {
            for (MultiString p : prod) {
                Products pro = new Products(p.getOne());
                pro.get();
                listModelProducts.addElement(pro);
            }
            jListProductsTrademark.setModel(listModelProducts);
        }
    }

    public void loadCategories(int trademark)
    {
        listModelCategories = new DefaultListModel();
        
        sql.rows.put("id_category", "");
        sql.wheres.put(new MultiString("id_trademark", Integer.toString(trademark)), new MultiString("=", "AND"));
        sql.wheres.put(new MultiString("enabled", "1"), new MultiString("=", ""));
        MultiString[] categ = sql.select("categories_trademark");
        if ( categ != null )
        {
            for (MultiString c : categ) {
                Category cat = new Category(Integer.parseInt(c.getOne()));
                cat.get();
                listModelCategories.addElement(cat);
            }
            jListCategoriesTrademark.setModel(listModelCategories);
        }
    }

    
    public void checkCategory(int category, int trademark)
    {
        boolean status = false;
        categoryTradeID = 0;
        sql.rows.put("id", "enabled");
        sql.wheres.put(new MultiString("id_trademark", Integer.toString(trademark)), new MultiString("=", "AND"));
        sql.wheres.put(new MultiString("id_category", Integer.toString(category)), new MultiString("=", ""));
        MultiString[] categ = sql.select("categories_trademark");
        if ( categ != null )
        {
            categoryTradeID = Integer.parseInt(categ[0].getOne());
            if ( categ[0].getTwo().contains("1") )
                status = true;
        }
        
        System.out.println( categoryTradeID + " | Cat: " + category + " - Tra: " + trademark + " = " + status);
        jCheckBoxCategory.setSelected(status);
    }

    public void checkProduct(String product, int trademark)
    {
        boolean status = false;
        sql.rows.put("id_trademark", "");
        sql.wheres.put(new MultiString("id", product), new MultiString("=", ""));
        MultiString[] products = sql.select("products");
        if ( products != null )
        {
            if ( products[0].getOne().contains(Integer.toString(trademark)) )
                status = true;
        }
        System.out.println( "Product: " + product + " - Tra: " + trademark + " = " + status);
        jCheckBoxProducts.setSelected(status);
    }

    public void checkProvider(int provider, int trademark)
    {
        boolean status = false;
        providerTradeID = 0;
        sql.rows.put("id", "enabled");
        sql.wheres.put(new MultiString("id_trademark", Integer.toString(trademark)), new MultiString("=", "AND"));
        sql.wheres.put(new MultiString("id_provider", Integer.toString(provider)), new MultiString("=", ""));
        MultiString[] providers = sql.select("providers_trademark");
        if ( providers != null )
        {
            providerTradeID = Integer.parseInt(providers[0].getOne());
            if ( providers[0].getTwo().contains("1") )
                    status = true;
        }
    
        System.out.println( providerTradeID + " | Provider: " + provider + " - Tra: " + trademark + " = " + status);
        jCheckBoxProviders.setSelected(status);
    }

    
    private void changeCatTra()
    {
        if ( (jComboBoxCategories.getSelectedIndex() != 0) && (jComboBoxTrademarkCat.getSelectedIndex() != 0) )
        {
            Category cat = (Category) ComboBoxModelCategories.getElementAt(jComboBoxCategories.getSelectedIndex());
            Trademark trad = (Trademark) ComboBoxModelTrademarks.getElementAt(jComboBoxTrademarkCat.getSelectedIndex());
            checkCategory(cat.getID(), trad.getID());
        }
    }

    private void changeProTra()
    {
        if ( (jComboBoxProducts.getSelectedIndex() != 0) && (jComboBoxTrademarkPro.getSelectedIndex() != 0) )
        {
            Products pro = (Products) ComboBoxModelProducts.getElementAt(jComboBoxProducts.getSelectedIndex());
            Trademark trad = (Trademark) ComboBoxModelTrademarksPro.getElementAt(jComboBoxTrademarkPro.getSelectedIndex());
            checkProduct(pro.getID(), trad.getID());
        }
    }

    private void changeProvTra()
    {
        if ( (jComboBoxProviders.getSelectedIndex() != 0) && (jComboBoxTrademarkProv.getSelectedIndex() != 0) )
        {
            Provider provider = (Provider) ComboBoxModelProviders.getElementAt(jComboBoxProviders.getSelectedIndex());
            Trademark trademark = (Trademark) ComboBoxModelTrademarksProv.getElementAt(jComboBoxTrademarkProv.getSelectedIndex());
            checkProvider(provider.getID(), trademark.getID());
        }
    }

    private void saveCatTra()
    {
        if ( (jComboBoxCategories.getSelectedIndex() != 0) && (jComboBoxTrademarkCat.getSelectedIndex() != 0) )
        {
            Category cat = (Category) ComboBoxModelCategories.getElementAt(jComboBoxCategories.getSelectedIndex());
            Trademark trad = (Trademark) ComboBoxModelTrademarks.getElementAt(jComboBoxTrademarkCat.getSelectedIndex());

            if ( jCheckBoxCategory.isSelected() )
                sql.rows.put("enabled", "1");
            else
                sql.rows.put("enabled", "0");
            sql.rows.put("id_category", Integer.toString(cat.getID()));
            sql.rows.put("id_trademark", Integer.toString(trad.getID()));
            sql.wheres.put(new MultiString("id", Integer.toString(categoryTradeID)), new MultiString("=", ""));
            if (!sql.update("categories_trademark")) {
                sql.insert("categories_trademark");
            }
            if ( jCheckBoxCategory.isSelected() )
                Init.MSG.showMessageInformation("Categoria & Marca enlazadas");
            else
                Init.MSG.showMessageInformation("Categoria & Marca desenlazadas");
            jComboBoxCategories.setSelectedIndex(0);
            jComboBoxTrademarkCat.setSelectedIndex(0);
            jCheckBoxCategory.setSelected(false);
        }
    }

    private void saveProTra()
    {
        if ( (jComboBoxProducts.getSelectedIndex() != 0) && (jComboBoxTrademarkPro.getSelectedIndex() != 0) )
        {
            Products pro = (Products) ComboBoxModelProducts.getElementAt(jComboBoxProducts.getSelectedIndex());
            Trademark trad = (Trademark) ComboBoxModelTrademarksPro.getElementAt(jComboBoxTrademarkPro.getSelectedIndex());

            String trademark = Integer.toString(trad.getID());

            if ( !jCheckBoxProducts.isSelected() )
            {
                trademark = "0";
            }

            sql.rows.put("id_trademark", trademark);
            sql.wheres.put(new MultiString("id", pro.getID()), new MultiString("=", ""));
            if (sql.update("products")) {
                if ( trademark != "0" )
                    Init.MSG.showMessageInformation("Producto & Marca enlazados");
                else
                    Init.MSG.showMessageInformation("Producto & Marca desenlazados");
            }
            jComboBoxProducts.setSelectedIndex(0);
            jComboBoxTrademarkPro.setSelectedIndex(0);
            jCheckBoxProducts.setSelected(false);
        }
    }

    private void saveProvTra()
    {
        if ( (jComboBoxProviders.getSelectedIndex() != 0) && (jComboBoxTrademarkProv.getSelectedIndex() != 0) )
        {
            Provider provider = (Provider) ComboBoxModelProviders.getElementAt(jComboBoxProviders.getSelectedIndex());
            Trademark trademark = (Trademark) ComboBoxModelTrademarksProv.getElementAt(jComboBoxTrademarkProv.getSelectedIndex());

            if ( jCheckBoxProviders.isSelected() )
                sql.rows.put("enabled", "1");
            else
                sql.rows.put("enabled", "0");
            
            sql.rows.put("id_provider", Integer.toString(provider.getID()));
            sql.rows.put("id_trademark", Integer.toString(trademark.getID()));
            sql.wheres.put(new MultiString("id", Integer.toString(providerTradeID)), new MultiString("=", ""));
            if (!sql.update("providers_trademark")) {
                sql.insert("providers_trademark");
            }
            if ( jCheckBoxProviders.isSelected() )
                Init.MSG.showMessageInformation("Proveedor & Marca enlazados");
            else
                Init.MSG.showMessageInformation("Proveedor & Marca desenlazados");
            jComboBoxProviders.setSelectedIndex(0);
            jComboBoxTrademarkProv.setSelectedIndex(0);
            jCheckBoxProviders.setSelected(false);
            
        }
    }
    

    private void addTrademark()
    {
        if ( !jTextFieldNameTrademark.getText().isEmpty() && !jTextFieldCodeTrademark.getText().isEmpty() )
        {
            Trademark tra = new Trademark(-1);
            tra.setName(jTextFieldNameTrademark.getText());
            tra.setCode(jTextFieldCodeTrademark.getText());
            tra.save();
            jTextFieldNameTrademark.setText("");
            jTextFieldCodeTrademark.setText("");
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddTrademark;
    private javax.swing.JButton jButtonDeleteTrademark;
    private javax.swing.JButton jButtonSaveCatTra;
    private javax.swing.JButton jButtonSaveProTra;
    private javax.swing.JButton jButtonSaveProvTra;
    private javax.swing.JButton jButtonTrademarkRefresh;
    private javax.swing.JCheckBox jCheckBoxCategory;
    private javax.swing.JCheckBox jCheckBoxProducts;
    private javax.swing.JCheckBox jCheckBoxProviders;
    private javax.swing.JComboBox<String> jComboBoxCategories;
    private javax.swing.JComboBox<String> jComboBoxProducts;
    private javax.swing.JComboBox<String> jComboBoxProviders;
    private javax.swing.JComboBox<String> jComboBoxTrademarkCat;
    private javax.swing.JComboBox<String> jComboBoxTrademarkPro;
    private javax.swing.JComboBox<String> jComboBoxTrademarkProv;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JList<String> jListCategoriesTrademark;
    private javax.swing.JList<String> jListProductsTrademark;
    private javax.swing.JList<String> jListTrademark;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanelTradeMarks;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTextFieldCodeTrademark;
    private javax.swing.JTextField jTextFieldNameTrademark;
    // End of variables declaration//GEN-END:variables
}
