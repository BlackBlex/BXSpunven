package frames.actions;

import bin.Main;
import com.blackblex.libs.application.components.styles.JButtonStyleFlat;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;
import com.blackblex.libs.system.utils.Observable;
import com.blackblex.libs.system.utils.Observer;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import utils.objects.Category;

public class CategoriesFrame extends javax.swing.JFrame implements Observer {
    
    private DefaultTableModel jtableCategoriesModel;
    private OperationSQL sql = Main.SQL;
    
    public CategoriesFrame() {
        initComponents();
        
        loadCategories();
        
        jTableCategories.setModel(jtableCategoriesModel);
        
        TableCellRenderer rendererFromHeader = jTableCategories.getTableHeader().getDefaultRenderer();
        JLabel headerLabel = (JLabel) rendererFromHeader;
        headerLabel.setHorizontalAlignment(JLabel.CENTER);
        
        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
        jTableCategories.getColumnModel().getColumn(0).setCellRenderer(dtcr);
        jTableCategories.getColumnModel().getColumn(1).setCellRenderer(dtcr);
        jTableCategories.getColumnModel().getColumn(2).setCellRenderer(dtcr);
        jTableCategories.setAutoCreateRowSorter(true);
        jTableCategories.getTableHeader().setReorderingAllowed(false);
        jTableCategories.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTableCategories.setRowHeight(35);
        
        JButtonStyleFlat jbuttonadd = new JButtonStyleFlat(jButtonAddCategory, Main.buttonColorAccept, 3);
        jButtonAddCategory.setUI(jbuttonadd);

        JButtonStyleFlat jbuttondelete = new JButtonStyleFlat(jButtonEditCategory, Main.buttonColorDecline, 3);
        jButtonEditCategory.setUI(jbuttondelete);
        
        this.setLocationRelativeTo(null);
        this.setTitle(Main.nameApp + " ~ Categorias");
    }

    private void loadCategories()
    {
        jtableCategoriesModel = new DefaultTableModel(new Object[][]{},
                new Object[]{"ID", "Categoria", "Descuento"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        
        sql.rows.put("id", "");
        MultiString[] categories = sql.select("categories");
        
        if ( categories != null )
        {
            for (MultiString category : categories) {
                Category cat = new Category(Integer.parseInt(category.getOne()));
                cat.get();
                jtableCategoriesModel.addRow(new Object[]{cat.getID(), cat.getName(), "%" + String.format("%.2f", cat.getDiscount())});
            }

            jTableCategories.setModel(jtableCategoriesModel);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableCategories = new javax.swing.JTable();
        jButtonAddCategory = new javax.swing.JButton();
        jButtonEditCategory = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new javax.swing.OverlayLayout(getContentPane()));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jTableCategories.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableCategoriesMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTableCategories);

        jButtonAddCategory.setText("Agregar");
        jButtonAddCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddCategoryActionPerformed(evt);
            }
        });

        jButtonEditCategory.setText("Editar");
        jButtonEditCategory.setEnabled(false);
        jButtonEditCategory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditCategoryActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButtonAddCategory, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonEditCategory, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButtonEditCategory, javax.swing.GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
                    .addComponent(jButtonAddCategory, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(12, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTableCategoriesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableCategoriesMouseClicked
        this.jButtonEditCategory.setEnabled(true);
    }//GEN-LAST:event_jTableCategoriesMouseClicked

    private void jButtonAddCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddCategoryActionPerformed
        CategoriesModifyFrame catmod = new CategoriesModifyFrame();
        catmod.observable.add(this);
        catmod.setVisible(true);
    }//GEN-LAST:event_jButtonAddCategoryActionPerformed

    private void jButtonEditCategoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditCategoryActionPerformed

        if ( jTableCategories.getSelectedRow() != -1 )
        {            
            Category cat = new Category(Integer.parseInt(jTableCategories.getValueAt(jTableCategories.getSelectedRow(), 0).toString()));

            CategoriesModifyFrame catmod = new CategoriesModifyFrame(cat);
            catmod.observable.add(this);
            catmod.setVisible(true);
        }
    }//GEN-LAST:event_jButtonEditCategoryActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        MainFrame.setOpenFrame(false);
    }//GEN-LAST:event_formWindowClosing

    @Override
    public void update(Observable obj) {
        loadCategories();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAddCategory;
    private javax.swing.JButton jButtonEditCategory;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTableCategories;
    // End of variables declaration//GEN-END:variables
}
