package frames.actions;

import frames.products.ProductsFrame;
import bin.Main;
import com.blackblex.libs.main.Init;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.Images;
import com.blackblex.libs.system.utils.MultiString;
import com.blackblex.libs.system.utils.Observable;
import com.blackblex.libs.system.utils.Observer;
import frames.BuyFrame;
import frames.people.CustomersFrame;
import frames.people.EmployeesFrame;
import frames.SellFrame;
import frames.people.ProvidersFrame;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.SwingDispatchService;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import utils.objects.Products;

public class MainFrame extends javax.swing.JFrame implements NativeKeyListener, WindowListener, Observer {

    public Images img = new Images();
    
    public static boolean openFrame = false;
    
    public MainFrame() {
        initComponents();
        GlobalScreen.setEventDispatcher(new SwingDispatchService());
        addWindowListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        
        
        this.setLocationRelativeTo(null);
        
        jLabelName.setText(Main.employee.getName());
        jLabelRank.setText(Main.employee.getRank().getName());
        jLabelAvatar.setIcon(img.getImagen(Main.avatarsPath + Main.employee.getID() + ".png", jLabelAvatar, new Dimension(40,40)));
        
        jLabelSells.setEnabled(Main.employee.getRank().getPermission().isCan_sells());
        jLabelBuys.setEnabled(Main.employee.getRank().getPermission().isCan_buys());
        jLabelProducts.setEnabled(Main.employee.getRank().getPermission().isCan_products());
        jLabelReports.setEnabled(Main.employee.getRank().getPermission().isCan_reports());
        jLabelEmployees.setEnabled(Main.employee.getRank().getPermission().isCan_employees());
        jLabelProviders.setEnabled(Main.employee.getRank().getPermission().isCan_providers());
        jLabelCustomers.setEnabled(Main.employee.getRank().getPermission().isCan_customers());
        jLabelSettings.setEnabled(Main.employee.getRank().getPermission().isCan_settings());
        jLabelTrademarks.setEnabled(Main.employee.getRank().getPermission().isCan_trademarks());
        jLabelCategories.setEnabled(Main.employee.getRank().getPermission().isCan_categories());
        
        /*
            Low stock
        */
        
        if ( Integer.parseInt(Main.config.getString("minimum_product_stock")) != 0 )
        {
            lowStock();
        }
        
        /*
            Low stock
        */
        
        this.setTitle(Main.nameApp);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelHeader = new javax.swing.JPanel();
        jPanelInfo = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabelAvatar = new javax.swing.JLabel();
        jLabelName = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabelRank = new javax.swing.JLabel();
        jPanelMenu = new javax.swing.JPanel();
        jLabelSells = new javax.swing.JLabel();
        jLabelBuys = new javax.swing.JLabel();
        jLabelProducts = new javax.swing.JLabel();
        jLabelReports = new javax.swing.JLabel();
        jLabelEmployees = new javax.swing.JLabel();
        jLabelProviders = new javax.swing.JLabel();
        jLabelCustomers = new javax.swing.JLabel();
        jPanelSettings = new javax.swing.JPanel();
        jLabelSettings = new javax.swing.JLabel();
        jPanelContent = new javax.swing.JPanel();
        jLabelLowStock = new javax.swing.JLabel();
        jPanelFooter = new javax.swing.JPanel();
        jLabelTrademarks = new javax.swing.JLabel();
        jLabelCategories = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanelHeader.setBackground(new java.awt.Color(35, 99, 142));
        jPanelHeader.setAlignmentX(0.0F);
        jPanelHeader.setPreferredSize(new java.awt.Dimension(632, 75));
        jPanelHeader.setLayout(new java.awt.GridBagLayout());

        jPanelInfo.setOpaque(false);
        jPanelInfo.setPreferredSize(new java.awt.Dimension(140, 70));
        jPanelInfo.setLayout(new javax.swing.BoxLayout(jPanelInfo, javax.swing.BoxLayout.Y_AXIS));

        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel1.setOpaque(false);
        jPanel1.setPreferredSize(new java.awt.Dimension(140, 50));
        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel1MouseClicked(evt);
            }
        });
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabelAvatar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/default.png"))); // NOI18N
        jLabelAvatar.setMinimumSize(new java.awt.Dimension(25, 25));
        jLabelAvatar.setPreferredSize(new java.awt.Dimension(25, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 15;
        gridBagConstraints.ipady = 15;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
        jPanel1.add(jLabelAvatar, gridBagConstraints);

        jLabelName.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabelName.setForeground(new java.awt.Color(255, 255, 255));
        jLabelName.setText("name");
        jLabelName.setPreferredSize(new java.awt.Dimension(60, 14));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 4);
        jPanel1.add(jLabelName, gridBagConstraints);
        jLabelName.getAccessibleContext().setAccessibleDescription("");

        jPanelInfo.add(jPanel1);

        jPanel2.setBackground(new java.awt.Color(72, 90, 102));

        jLabelRank.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelRank.setForeground(new java.awt.Color(255, 255, 255));
        jLabelRank.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelRank.setText("rank");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabelRank, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabelRank, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
        );

        jPanelInfo.add(jPanel2);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelHeader.add(jPanelInfo, gridBagConstraints);

        jPanelMenu.setBackground(new java.awt.Color(255, 255, 255));
        jPanelMenu.setPreferredSize(new java.awt.Dimension(440, 70));
        jPanelMenu.setLayout(new java.awt.GridBagLayout());

        jLabelSells.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelSells.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/sells.png"))); // NOI18N
        jLabelSells.setText("Ventas");
        jLabelSells.setToolTipText("Acceso rápido F1");
        jLabelSells.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabelSells.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelSells.setMaximumSize(new java.awt.Dimension(60, 65));
        jLabelSells.setMinimumSize(new java.awt.Dimension(60, 65));
        jLabelSells.setPreferredSize(new java.awt.Dimension(60, 65));
        jLabelSells.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jLabelSells.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelSellsMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelMenu.add(jLabelSells, gridBagConstraints);

        jLabelBuys.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelBuys.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/buys.png"))); // NOI18N
        jLabelBuys.setText("Compras");
        jLabelBuys.setToolTipText("Acceso rápido F2");
        jLabelBuys.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabelBuys.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelBuys.setMaximumSize(new java.awt.Dimension(60, 65));
        jLabelBuys.setMinimumSize(new java.awt.Dimension(60, 65));
        jLabelBuys.setPreferredSize(new java.awt.Dimension(60, 65));
        jLabelBuys.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jLabelBuys.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelBuysMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelMenu.add(jLabelBuys, gridBagConstraints);

        jLabelProducts.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelProducts.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/products.png"))); // NOI18N
        jLabelProducts.setText("Productos");
        jLabelProducts.setToolTipText("Acceso rápido F3");
        jLabelProducts.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabelProducts.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelProducts.setMaximumSize(new java.awt.Dimension(60, 65));
        jLabelProducts.setMinimumSize(new java.awt.Dimension(60, 65));
        jLabelProducts.setPreferredSize(new java.awt.Dimension(60, 65));
        jLabelProducts.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jLabelProducts.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelProductsMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelMenu.add(jLabelProducts, gridBagConstraints);

        jLabelReports.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelReports.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/reports.png"))); // NOI18N
        jLabelReports.setText("Reportes");
        jLabelReports.setToolTipText("Acceso rápido F4");
        jLabelReports.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabelReports.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelReports.setMaximumSize(new java.awt.Dimension(60, 65));
        jLabelReports.setMinimumSize(new java.awt.Dimension(60, 65));
        jLabelReports.setPreferredSize(new java.awt.Dimension(60, 65));
        jLabelReports.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jLabelReports.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelReportsMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelMenu.add(jLabelReports, gridBagConstraints);

        jLabelEmployees.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelEmployees.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/employees.png"))); // NOI18N
        jLabelEmployees.setText("Empleados");
        jLabelEmployees.setToolTipText("Acceso rápido F5");
        jLabelEmployees.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabelEmployees.setFocusable(false);
        jLabelEmployees.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelEmployees.setMaximumSize(new java.awt.Dimension(60, 65));
        jLabelEmployees.setMinimumSize(new java.awt.Dimension(60, 65));
        jLabelEmployees.setPreferredSize(new java.awt.Dimension(60, 65));
        jLabelEmployees.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jLabelEmployees.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelEmployeesMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelMenu.add(jLabelEmployees, gridBagConstraints);

        jLabelProviders.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelProviders.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/providers.png"))); // NOI18N
        jLabelProviders.setText("Proveedores");
        jLabelProviders.setToolTipText("Acceso rápido F6");
        jLabelProviders.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabelProviders.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelProviders.setMaximumSize(new java.awt.Dimension(65, 65));
        jLabelProviders.setMinimumSize(new java.awt.Dimension(65, 65));
        jLabelProviders.setPreferredSize(new java.awt.Dimension(65, 65));
        jLabelProviders.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jLabelProviders.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelProvidersMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelMenu.add(jLabelProviders, gridBagConstraints);

        jLabelCustomers.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelCustomers.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/customers.png"))); // NOI18N
        jLabelCustomers.setText("Clientes");
        jLabelCustomers.setToolTipText("Acceso rápido F7");
        jLabelCustomers.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabelCustomers.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelCustomers.setMaximumSize(new java.awt.Dimension(60, 65));
        jLabelCustomers.setMinimumSize(new java.awt.Dimension(60, 65));
        jLabelCustomers.setPreferredSize(new java.awt.Dimension(60, 65));
        jLabelCustomers.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jLabelCustomers.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelCustomersMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelMenu.add(jLabelCustomers, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 0, 0, 0);
        jPanelHeader.add(jPanelMenu, gridBagConstraints);

        jPanelSettings.setOpaque(false);
        jPanelSettings.setPreferredSize(new java.awt.Dimension(140, 70));
        jPanelSettings.setLayout(new java.awt.GridBagLayout());

        jLabelSettings.setForeground(new java.awt.Color(255, 255, 255));
        jLabelSettings.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelSettings.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/settings.png"))); // NOI18N
        jLabelSettings.setText("Configuración");
        jLabelSettings.setToolTipText("Acceso rápido F8");
        jLabelSettings.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabelSettings.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelSettings.setMaximumSize(new java.awt.Dimension(70, 60));
        jLabelSettings.setMinimumSize(new java.awt.Dimension(70, 60));
        jLabelSettings.setPreferredSize(new java.awt.Dimension(70, 60));
        jLabelSettings.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jLabelSettings.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelSettingsMouseClicked(evt);
            }
        });
        jPanelSettings.add(jLabelSettings, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        jPanelHeader.add(jPanelSettings, gridBagConstraints);

        getContentPane().add(jPanelHeader, java.awt.BorderLayout.PAGE_START);

        jPanelContent.setBackground(new java.awt.Color(255, 255, 255));

        jLabelLowStock.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelLowStock.setToolTipText("Lista de productos de bajo stock, favor de reportar al dueño o al empleado encargado.");
        jLabelLowStock.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabelLowStock.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelLowStock.setVerticalTextPosition(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout jPanelContentLayout = new javax.swing.GroupLayout(jPanelContent);
        jPanelContent.setLayout(jPanelContentLayout);
        jPanelContentLayout.setHorizontalGroup(
            jPanelContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelContentLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelLowStock, javax.swing.GroupLayout.DEFAULT_SIZE, 700, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanelContentLayout.setVerticalGroup(
            jPanelContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelContentLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelLowStock, javax.swing.GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
                .addContainerGap())
        );

        getContentPane().add(jPanelContent, java.awt.BorderLayout.CENTER);

        jPanelFooter.setBackground(new java.awt.Color(204, 204, 204));
        jPanelFooter.setMinimumSize(new java.awt.Dimension(720, 40));
        jPanelFooter.setLayout(new java.awt.GridBagLayout());

        jLabelTrademarks.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelTrademarks.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/trademark.png"))); // NOI18N
        jLabelTrademarks.setText("Marcas");
        jLabelTrademarks.setToolTipText("Acceso rápido F9");
        jLabelTrademarks.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabelTrademarks.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelTrademarks.setMaximumSize(new java.awt.Dimension(60, 65));
        jLabelTrademarks.setMinimumSize(new java.awt.Dimension(60, 65));
        jLabelTrademarks.setPreferredSize(new java.awt.Dimension(60, 65));
        jLabelTrademarks.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jLabelTrademarks.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelTrademarksMouseClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 20);
        jPanelFooter.add(jLabelTrademarks, gridBagConstraints);

        jLabelCategories.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelCategories.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/categories.png"))); // NOI18N
        jLabelCategories.setText("Categorias");
        jLabelCategories.setToolTipText("Acceso rápido F10");
        jLabelCategories.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabelCategories.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabelCategories.setMaximumSize(new java.awt.Dimension(60, 65));
        jLabelCategories.setMinimumSize(new java.awt.Dimension(60, 65));
        jLabelCategories.setPreferredSize(new java.awt.Dimension(60, 65));
        jLabelCategories.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jLabelCategories.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelCategoriesMouseClicked(evt);
            }
        });
        jPanelFooter.add(jLabelCategories, new java.awt.GridBagConstraints());

        getContentPane().add(jPanelFooter, java.awt.BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void setOpenFrame(boolean o)
    {
        openFrame = o;
    }
    
    private void lowStock()
    {
        OperationSQL sql = Main.SQL;

        sql.rows.put("id", "");
        sql.wheres.put(new MultiString("stock", Main.config.getString("minimum_product_stock")), new MultiString("<=", ""));
        MultiString[] products = sql.select("products");

        if ( products != null )
        {
            jLabelLowStock.setText("<html><center><span style='font-size:16px'><strong>Productos con stock bajo</strong></span></center>"
                + "<br>");
            for ( MultiString product : products)
            {    
                Products pro = new Products(product.getOne());
                pro.get();
                jLabelLowStock.setText(jLabelLowStock.getText() + "<span style='font-size:12px'>" + pro.getDescription() + " ~ " + pro.getPresentation() + "[" + pro.getModel() + "]" + "</span><br><br>");
            }

            jLabelLowStock.setText(jLabelLowStock.getText() + "</html>");
        }
    }
    
    private void jLabelSellsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelSellsMouseClicked
        sells();
    }//GEN-LAST:event_jLabelSellsMouseClicked

    private void jLabelBuysMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelBuysMouseClicked
        buys();
    }//GEN-LAST:event_jLabelBuysMouseClicked

    private void jLabelProductsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelProductsMouseClicked
        products();
    }//GEN-LAST:event_jLabelProductsMouseClicked

    private void jLabelReportsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelReportsMouseClicked
        reports();
    }//GEN-LAST:event_jLabelReportsMouseClicked

    private void jLabelEmployeesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelEmployeesMouseClicked
        employees();
    }//GEN-LAST:event_jLabelEmployeesMouseClicked

    private void jLabelProvidersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelProvidersMouseClicked
        providers();
    }//GEN-LAST:event_jLabelProvidersMouseClicked

    private void jLabelCustomersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelCustomersMouseClicked
        customers();
    }//GEN-LAST:event_jLabelCustomersMouseClicked

    private void jLabelSettingsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelSettingsMouseClicked
        settings();
    }//GEN-LAST:event_jLabelSettingsMouseClicked

    private void jPanel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseClicked
        
        if ( Init.MSG.showConfirmInformation("¿Quiere salir del sistema?") )
        {
            this.openFrame = true;
            new LoginFrame().setVisible(true);
            this.setVisible(false);
        }
    }//GEN-LAST:event_jPanel1MouseClicked

    private void jLabelTrademarksMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelTrademarksMouseClicked
        trademarks();
    }//GEN-LAST:event_jLabelTrademarksMouseClicked

    private void jLabelCategoriesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelCategoriesMouseClicked
        categories();
    }//GEN-LAST:event_jLabelCategoriesMouseClicked

    private void sells()
    {
        if ( Main.employee.getRank().getPermission().isCan_sells() )
        {
            System.out.println("sells");
            this.openFrame = true;
            SellFrame sellframe = new SellFrame();
            sellframe.observable.add(this);
            sellframe.setVisible(true);
        }
    }
    
    private void buys()
    {
        if ( Main.employee.getRank().getPermission().isCan_buys() )
        {
            System.out.println("buys");
            this.openFrame = true;
            BuyFrame buyframe = new BuyFrame();
            buyframe.observable.add(this);
            buyframe.setVisible(true);
        }
    }
    
    private void products()
    {
        if ( Main.employee.getRank().getPermission().isCan_products() )
        {
            System.out.println("products");
            this.openFrame = true;
            ProductsFrame product = new ProductsFrame();
            product.observable.add(this);
            product.setVisible(true);
        }
    }
    
    private void reports()
    {
        if ( Main.employee.getRank().getPermission().isCan_reports() )
        {
            System.out.println("reports");
            this.openFrame = true;
            new ReportsFrame().setVisible(true);
        }
    }
    
    private void employees()
    {
        if ( Main.employee.getRank().getPermission().isCan_employees() )
        {
            System.out.println("employees");
            this.openFrame = true;
            new EmployeesFrame().setVisible(true);
        }
    }
        
    private void providers()
    {
        if ( Main.employee.getRank().getPermission().isCan_providers() )
        {
            System.out.println("providers");
            this.openFrame = true;
            new ProvidersFrame().setVisible(true);
        }
    }
    
    private void customers()
    {
        if ( Main.employee.getRank().getPermission().isCan_customers() )
        {
            System.out.println("customers");
            this.openFrame = true;
            new CustomersFrame().setVisible(true);
        }
    }
    
    private void settings()
    {
        if ( Main.employee.getRank().getPermission().isCan_settings() )
        {
            System.out.println("settings");
            this.openFrame = true;
            new SettingsFrame().setVisible(true);
        }
    }
    
    private void trademarks()
    {
        if ( Main.employee.getRank().getPermission().isCan_trademarks() )
        {
            System.out.println("trademarks");
            this.openFrame = true;
            new TrademarkFrame().setVisible(true);
        }
    }
    
    private void categories()
    {
        if ( Main.employee.getRank().getPermission().isCan_categories() )
        {
            System.out.println("categories");
            this.openFrame = true;
            new CategoriesFrame().setVisible(true);
        }
    }
    
    @Override
    public void update(Observable obj) {
        lowStock();
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelAvatar;
    private javax.swing.JLabel jLabelBuys;
    private javax.swing.JLabel jLabelCategories;
    private javax.swing.JLabel jLabelCustomers;
    private javax.swing.JLabel jLabelEmployees;
    private javax.swing.JLabel jLabelLowStock;
    private javax.swing.JLabel jLabelName;
    private javax.swing.JLabel jLabelProducts;
    private javax.swing.JLabel jLabelProviders;
    private javax.swing.JLabel jLabelRank;
    private javax.swing.JLabel jLabelReports;
    private javax.swing.JLabel jLabelSells;
    private javax.swing.JLabel jLabelSettings;
    private javax.swing.JLabel jLabelTrademarks;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanelContent;
    private javax.swing.JPanel jPanelFooter;
    private javax.swing.JPanel jPanelHeader;
    private javax.swing.JPanel jPanelInfo;
    private javax.swing.JPanel jPanelMenu;
    private javax.swing.JPanel jPanelSettings;
    // End of variables declaration//GEN-END:variables

    @Override
    public void windowOpened(WindowEvent e) {
        // Initialze native hook.
        try {
            GlobalScreen.registerNativeHook();
        }
        catch (NativeHookException ex) {
            System.err.println("There was a problem registering the native hook.");
            System.err.println(ex.getMessage());
            ex.printStackTrace();

            System.exit(1);
        }

        GlobalScreen.addNativeKeyListener(this);
    }

    @Override
    public void windowClosed(WindowEvent e) {
        try {
            //Clean up the native hook.
            GlobalScreen.unregisterNativeHook();
            System.runFinalization();
            System.exit(0);
        } catch (NativeHookException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void windowClosing(WindowEvent e) { /* Unimplemented */ }
    @Override
    public void windowIconified(WindowEvent e) { /* Unimplemented */ }
    @Override
    public void windowDeiconified(WindowEvent e) { /* Unimplemented */ }
    @Override
    public void windowActivated(WindowEvent e) { /* Unimplemented */ }
    @Override
    public void windowDeactivated(WindowEvent e) { /* Unimplemented */ }

    @Override
    public void nativeKeyReleased(NativeKeyEvent e) {
        if ( !this.openFrame )
        {
            switch (e.getKeyCode()) {
                case NativeKeyEvent.VC_F1:
                    sells();
                    break;
                case NativeKeyEvent.VC_F2:
                    buys();
                    break;
                case NativeKeyEvent.VC_F3:
                    products();
                    break;
                case NativeKeyEvent.VC_F4:
                    reports();
                    break;
                case NativeKeyEvent.VC_F5:            
                    employees();
                    break;
                case NativeKeyEvent.VC_F6:
                    providers();
                    break;
                case NativeKeyEvent.VC_F7:
                    customers();
                    break;
                case NativeKeyEvent.VC_F8:
                    settings();
                    break;
                case NativeKeyEvent.VC_F9:
                    trademarks();
                    break;
                case NativeKeyEvent.VC_F10:
                    categories();
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent e) { /* Unimplemented */ }
    @Override
    public void nativeKeyTyped(NativeKeyEvent e) { /* Unimplemented */ }
    
}
