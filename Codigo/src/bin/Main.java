package bin;

import com.blackblex.libs.core.file.FileProperties;
import com.blackblex.libs.core.file.Files;
import com.blackblex.libs.main.Init;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.Console;
import com.blackblex.libs.system.utils.Security;
import com.blackblex.libs.system.utils.SystemVariables;
import utils.objects.Employee;
import frames.actions.LoginFrame;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.jnativehook.GlobalScreen;

public class Main {

    public static final String
            nameApp         = "Papeleria - Spunven [1a0]",
            street          = "Calle 32 #21 Col. Cants",
            street2         = "Avenida Jorge Lopez",
            DS              = System.getProperty("file.separator"),
            pathApp         = System.getProperty("user.dir") + DS,
            resourcesPath   = "/resources/",
            imagesPath      = resourcesPath + "img/",
            avatarsPath     = imagesPath + "avatars/";
    
    public static String
            buttonColorAccept       = "#5cb85c",
            buttonColorDecline      = "#B85C5C",
            buttonColorAccept2      = "#27ae60",
            buttonColorDecline2     = "#AE2727",
            buttonColorSearch       = "#5bc0de",
            buttonColorSave         = "#2980B9",
            buttonColorSecundary    = "#23638e",
            comboboxColorPeople     = "#4e5d6c";

    private static SystemVariables v;
    public static FileProperties config;
    public static OperationSQL SQL;

    public static Employee employee;

    public static void main(String[] args) {
        LogManager.getLogManager().reset();

        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);
        v = new SystemVariables();

        config = new FileProperties("settings.cfg");

        try {
            JFrame.setDefaultLookAndFeelDecorated(true);
            javax.swing.UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            System.out.println("[Main]: " + ex.getMessage());
        }
        
        new InstallSystem();

        SQL = new OperationSQL("database.db");
        
        new LoginFrame().setVisible(true);
    }
    
    

}

class InstallSystem
{
    private Security sec = new Security();
    private final String password = Init.getRandomCode(6);
    
    private String
            buyTable                    = "CREATE TABLE buys (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_buy VARCHAR (9) NOT NULL, date DATE NOT NULL, time TIME NOT NULL, total DECIMAL (2, 10) NOT NULL, id_provider INTEGER NOT NULL);",
            buyDetailsTable             = "CREATE TABLE buys_details (id INTEGER PRIMARY KEY AUTOINCREMENT, id_buy VARCHAR (9), units INT (11), id_product VARCHAR (12) );",
            categoriesTable             = "CREATE TABLE categories (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name VARCHAR (20)   NOT NULL, discount DECIMAL (2, 4) NOT NULL);",
            categoriesTrademarkTable    = "CREATE TABLE categories_trademark (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_category  INTEGER NOT NULL, id_trademark INTEGER NOT NULL, enabled INT (2) NOT NULL DEFAULT '0');",
            customerTable               = "CREATE TABLE customers (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name VARCHAR (30) NOT NULL, street VARCHAR (50) NOT NULL, telephone BIGINT (12) NOT NULL, spent DECIMAL (2, 10) NOT NULL);",
            employeeTable               = "CREATE TABLE employees (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name VARCHAR (30) NOT NULL, lastname VARCHAR (40) NOT NULL, user VARCHAR (12) NOT NULL, email VARCHAR (40) NOT NULL, pass VARCHAR (62) NOT NULL, rank INTEGER NOT NULL);",
            employeeInsert              = "INSERT INTO employees (name, lastname, user, email, pass, rank) VALUES ('Admin', 'Admin', 'admin', 'admin@localhost',",
            productTable                = "CREATE TABLE products (id VARCHAR (12) NOT NULL, description varchar (100) NOT NULL, model varchar (50) NOT NULL, presentation varchar (100) NOT NULL, price decimal (2, 10) NOT NULL, stock int (11) NOT NULL, id_category INTEGER NOT NULL, id_trademark INTEGER NOT NULL DEFAULT '0', idauto INTEGER PRIMARY KEY AUTOINCREMENT);",
            //productProviderTable        = "CREATE TABLE products_providers (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_product VARCHAR (12) NOT NULL, id_provider INTEGER NOT NULL, comment varchar (150) NOT NULL);",
            providerTable               = "CREATE TABLE providers (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name varchar (100) NOT NULL, owner varchar (20) NOT NULL, street varchar (50) NOT NULL, telephone bigint (12) NOT NULL, id_category INTEGER NOT NULL DEFAULT (0), code int (4) NOT NULL);",
            providerTrademarkTable      = "CREATE TABLE providers_trademark (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_provider INTEGER NOT NULL, id_trademark INTEGER NOT NULL, enabled INT (2) DEFAULT (0));",
            rankTable                   = "CREATE TABLE ranks (id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR (20));",
            rankInsert                  = "INSERT INTO ranks (id, name) VALUES (1, 'Dueño'); INSERT INTO ranks (id, name) VALUES (2, 'Empleado');",
            rankPermissionTable         = "CREATE TABLE ranks_permissions (id INTEGER PRIMARY KEY AUTOINCREMENT, id_rank INTEGER, can_sells BOOLEAN DEFAULT false, can_buys BOOLEAN DEFAULT false, can_products BOOLEAN DEFAULT false, can_reports BOOLEAN DEFAULT false, can_employees BOOLEAN DEFAULT false, can_providers BOOLEAN DEFAULT false, can_customers BOOLEAN DEFAULT false, can_settings BOOLEAN DEFAULT false, can_trademarks BOOLEAN DEFAULT false, can_categories BOOLEAN DEFAULT false);",
            rankPermissionInsert        = "INSERT INTO ranks_permissions (id, id_rank, can_sells, can_buys, can_products, can_reports, can_employees, can_providers, can_customers, can_settings, can_trademarks, can_categories) VALUES (1, 1, 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true', 'true'); INSERT INTO ranks_permissions (id, id_rank, can_sells, can_buys, can_products, can_reports, can_employees, can_providers, can_customers, can_settings, can_trademarks, can_categories) VALUES (2, 2, 'true', 'true', 'false', 'true', 'false', 'false', 'false', 'false', 'false', 'false');",
            sellTable                   = "CREATE TABLE sells (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_sell varchar (9) NOT NULL, date date NOT NULL, time time NOT NULL, total decimal (2, 10) NOT NULL, id_customer INTEGER NOT NULL, id_employee INTEGER);",
            sellDetailsTable            = "CREATE TABLE sells_details (id INTEGER PRIMARY KEY AUTOINCREMENT, id_sell VARCHAR (9), units INTEGER (11), id_product VARCHAR (12), discount DECIMAL (2, 4));",
            trademarkTable              = "CREATE TABLE trademark (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name varchar (50) NOT NULL, code int (3) NOT NULL);",
            
            triggers                    = "CREATE TRIGGER delete_all_trademark BEFORE DELETE ON trademark FOR EACH ROW BEGIN DELETE FROM providers_trademark WHERE id_trademark = old.id; UPDATE products SET id_trademark = 0 WHERE id_trademark = old.id; DELETE FROM categories_trademark WHERE id_trademark = old.id; END; CREATE TRIGGER delete_all_category BEFORE DELETE ON categories FOR EACH ROW BEGIN DELETE FROM categories_trademark WHERE id_category = old.id; UPDATE products SET id_category = 0, id_trademark = 0 WHERE id_category = old.id; UPDATE providers SET id_category = 0 WHERE id_category = old.id; END;";
    
    
    public InstallSystem()
    {
        sec.addKey("BlackBlex");
        employeeInsert = employeeInsert + " '" + sec.encriptar(password) + "', 1);";
            
        String table = "database.db";
        OperationSQL sql = new OperationSQL(table);
        boolean flag = false;
        int countError = -1;
        Files file = new Files();
        file.init(table);
        
        Files directoryTickets = new Files();
        Files directoryReports = new Files();
        Files directorySells = new Files();
        Files directoryBuys = new Files();
        
        directoryTickets.init("tickets");
        directoryReports.init("reports");
        directorySells.init("tickets/sells");
        directoryBuys.init("tickets/buys");
        
        if ( !directoryTickets.exist() )
        {
            if ( directoryTickets.mkdir() )
            {
                Console.out.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                Console.out.println("Ticket folder is begin installed...");
                if ( directorySells.mkdir() )
                    Console.out.println("Sell folder is begin installed...");
                if ( directoryBuys.mkdir() )
                    Console.out.println("Buy folder is begin installed...");
            }
            else
                countError++;
        }
        
        if ( !directoryReports.exist() )
        {
            if ( directoryReports.mkdir() )
            {
                Console.out.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                Console.out.println("Report folder is begin installed...");
            }
            else
                countError++;
        }
        
        if ( !file.exist() )
        {
            Console.out.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            Console.out.println("System is being installed...");
            if ( sql.exec(this.buyTable) )
                flag = true;
            else
                countError++;
            Console.out.println("Creating buy table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.buyDetailsTable) )
                flag = true;
            else
                countError++;
            Console.out.println("Creating buy details table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.categoriesTable) )
                flag = true;
            else
                countError++;
            Console.out.println("Creating categories table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.categoriesTrademarkTable) )
                flag = true;
            else
                countError++;
            Console.out.println("Creating categories_trademark table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.customerTable) )
                flag = true;
            else
                countError++;
            Console.out.println("Creating customers table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.employeeTable) )
                flag = true;
            else
                countError++;
            Console.out.println("Creating employees table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.employeeInsert) )
                flag = true;
            else
                countError++;
            Console.out.println("Inserting default employee [admin:" + password + "] ... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.productTable) )
                flag = true;
            else
                countError++;
            Console.out.println("Creating products table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.providerTable) )
                flag = true;
            else
                countError++;
            Console.out.println("Creating providers table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.providerTrademarkTable) )
                flag = true;
            else
                countError++;
            Console.out.println("Creating provider_trademark table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.rankTable) )
                flag = true;
            else
                countError++;
            Console.out.println("Creating ranks table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.rankInsert) )
                flag = true;
            else
                countError++;
            Console.out.println("Inserting defaults ranks [Owner:Employee] table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.rankPermissionTable) )
                flag = true;
            else
                countError++;
            Console.out.println("Creating ranks_permissions table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.rankPermissionInsert) )
                flag = true;
            else
                countError++;
            Console.out.println("Inserting defaults ranks_permissions [Owner:Employee] table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.sellTable) )
                flag = true;
            else
                countError++;
            Console.out.println("Creating sells table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.sellDetailsTable) )
                flag = true;
            else
                countError++;
            Console.out.println("Creating sells_details table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.trademarkTable) )
                flag = true;
            else
                countError++;
            Console.out.println("Creating trademarks table... [" + flag + "]");
            flag = false;
            
            if ( sql.exec(this.triggers) )
                flag = true;
            else
                countError++;
            Console.out.println("Inserting triggers... [" + flag + "]");
            flag = false;
            
            if ( countError != -1 )
                Console.out.println( file.delete() );
            else
                countError = 0;
        }
        
        if ( countError > 0 )
        {
            Init.MSG.showMessageError("Ocurrio un error al instalar el sistema, el programa se cerrará");
            Console.out.println( directorySells.delete() );
            Console.out.println( directoryBuys.delete() );
            Console.out.println( directoryTickets.delete() );
            Console.out.println( directoryReports.delete() );
            System.exit(-1);
        }
        else if ( countError != -1 )
            Console.out.println("Instalación completa, favor de cerrar esta ventana.");
    }
}