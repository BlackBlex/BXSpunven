package utils.objects;

import com.blackblex.libs.main.Init;
import com.blackblex.libs.system.utils.MultiString;

public class Customer extends Person {

    private boolean ERROR_SPENT;

    private double spent;

    public Customer(int id) {
        super(id);
    }
    
    public String getAvatar()
    {
        return "resources\\img\\customers\\" + this.ID + ".png";
    }

    public double getSpent() {
        return spent;
    }

    public void setSpent(double s) {
        spent = s;
    }

    @Override
    public void get() {
        sql.rows.put("name", "street");
        sql.rows.put("telephone", "spent");
        sql.wheres.put(new MultiString("id", Integer.toString(ID)), new MultiString("=", ""));
        MultiString[] result = sql.select("customers");

        for (MultiString customer : result) {
            setName(customer.getOne());
            setStreet(customer.getTwo());
            setTelephone(Long.parseLong(customer.getThree()));
            setSpent(Double.parseDouble(customer.getFour()));
        }
    }

    @Override
    public void resetError() {
        super.resetError();
        this.ERROR_SPENT = false;
    }

    @Override
    public boolean isErrors() {
        boolean temp = super.isErrors();

        if (temp) {
            return true;
        } else {
            if (this.ERROR_SPENT) {
                return true;
            }
            return false;
        }
    }

    @Override
    public boolean save() {
        boolean temp = super.save();

        if (temp) {
            if (!Init.isNumeric(Double.toString(this.spent), "D")) {
                this.ERROR_SPENT = true;
            }

            if (!isErrors()) {
                sql.rows.put("name", name);
                sql.rows.put("street", street);
                sql.rows.put("telephone", Long.toString(telephone));
                sql.rows.put("spent", Double.toString(spent));
                sql.wheres.put(new MultiString("id", Integer.toString(ID)), new MultiString("=", ""));

                if (!sql.update("customers")) {
                    sql.insert("customers");
                }

                sql.clearAll();
                return true;
            } else {
                if (this.ERROR_SPENT) {
                    Init.MSG.showMessageError("El dinero gastado parece ser invalido!.\nRecuerda que solo se permiten numeros con decimales.");
                }
                resetError();
                return false;
            }
        } else {
            return false;
        }
    }
    
    public boolean delete() {
        sql.wheres.put(new MultiString("id", Integer.toString(this.ID)), new MultiString("=", ""));
        return sql.delete("customers");
    }
}
