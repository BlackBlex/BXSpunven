package utils.objects;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;
import utils.objects.BuyTransaction.BuyDetail;
import utils.objects.SellTransaction.SellDetail;

public class Report {

    private float pageWidth = 0;
    private float pageHeight = 0;
    private PdfPTable documentRoot;
    //private Document documentRooot;

    private Paragraph paragraph = null;
    private PdfPCell cell = null;
    private PdfPTable table = null;
    
    private int option;

    private String
            fecha,
            hora,
            rango1,
            rango2,
            usuario,
            id;

    private ArrayList items;

    private Font fontTitle = FontFactory.getFont(FontFactory.COURIER_BOLD.toString(), 26, Font.NORMAL);
    private Font fontContenido = FontFactory.getFont(FontFactory.COURIER.toString(), 11, Font.NORMAL);
    
    public Report(String id, String fecha, String hora, String rango1, String rango2, String usuario, int o, ArrayList it) {
        this.option = o;
        this.fecha = fecha;
        this.hora = hora;
        this.rango1 = rango1;
        this.rango2 = rango2;
        this.usuario = usuario;
        this.items = it;
        this.id = id;

        this.pageWidth = 216;
        this.pageHeight = 279;

        this.documentRoot = new PdfPTable(1);
        this.documentRoot.setTotalWidth(PageSize.A4.getWidth() - 10);
        this.documentRoot.setLockedWidth(true);
    }

    private void setParagraph() {
        this.paragraph = new Paragraph();
    }

    private void addParagraph(Phrase pa) {
        if (this.paragraph != null) {
            this.paragraph.add(pa);
        }
    }

    private void flushParagraph() {
        if (this.paragraph != null) {
            PdfPCell cell = new PdfPCell(this.paragraph);
            cell.setBorderColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            this.documentRoot.addCell(cell);
        }
    }
    private void flushParagraphTitle() {
        if (this.paragraph != null) {
            PdfPCell cell = new PdfPCell(this.paragraph);
            //cell.setBorderColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorder(Rectangle.BOTTOM | Rectangle.TOP);
            cell.setBorderWidth(2f);
            this.documentRoot.addCell(cell);
        }
    }

    private void setCell(int t) {
        this.table = new PdfPTable(t);
        this.table.getDefaultCell().setBorderWidth(0);
        this.documentRoot.getDefaultCell().setBorderWidth(0);
        this.documentRoot.getDefaultCell().setBorderColor(BaseColor.WHITE);
    }

    private void addCell(Phrase pa, BaseColor co, int h, int col) {
        if (this.table != null) {
            this.cell = new PdfPCell(pa);
            this.cell.setColspan(col);
            this.cell.setFixedHeight(15);
            this.cell.setBorderColor(co);
            this.cell.setBorderWidth(0);
            this.cell.setHorizontalAlignment(h);
            this.table.addCell(this.cell);
        }
    }

    private void addCell(Phrase pa, BaseColor co, int h, int col, int border) {
        if (this.table != null) {
            this.cell = new PdfPCell(pa);
            this.cell.setColspan(col);
            this.cell.setFixedHeight(20);
            this.cell.setBorderColor(co);
            this.cell.setBorderWidth(border);
            this.cell.setHorizontalAlignment(h);
            this.table.addCell(this.cell);
        }
    }

    private void flushCell() {
        if (this.table != null) {
            this.documentRoot.addCell(this.table);
        }
    }

    public void process() {
        setParagraph();
            if ( this.option == 1 )
                addParagraph(new Phrase("Reporte de compras", fontTitle));
            else
                addParagraph(new Phrase("Reporte de ventas", fontTitle));
                
            addParagraph(new Phrase(Chunk.NEWLINE));
            addParagraph(new Phrase(Chunk.NEWLINE));
        flushParagraphTitle();
        
        setParagraph();
            addParagraph(new Phrase(Chunk.NEWLINE));
        flushParagraph();

        setCell(4);
            addCell(new Phrase("Reporte:", fontContenido), BaseColor.WHITE, Element.ALIGN_RIGHT, 2);
            addCell(new Phrase("#" + this.id, fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 5);
            addCell(new Phrase("Fecha:", fontContenido), BaseColor.WHITE, Element.ALIGN_RIGHT, 2);
            addCell(new Phrase(this.fecha, fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 5);
            addCell(new Phrase("Hora:", fontContenido), BaseColor.WHITE, Element.ALIGN_RIGHT, 2);
            addCell(new Phrase(this.hora, fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 5);
            addCell(new Phrase("Realizado por:", fontContenido), BaseColor.WHITE, Element.ALIGN_RIGHT, 2);
            addCell(new Phrase(this.usuario, fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 5);
            addCell(new Phrase("Reporte de la fecha:", fontContenido), BaseColor.WHITE, Element.ALIGN_RIGHT, 2);
            addCell(new Phrase(this.rango1 + " a " + this.rango2, fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 5);
        flushCell();

        setParagraph();
            addParagraph(new Phrase(new Chunk(new DottedLineSeparator())));
            addParagraph(new Phrase(Chunk.NEWLINE));
            addParagraph(new Phrase(Chunk.NEWLINE));
        flushParagraph();

        setCell(5);
            addCell(new Phrase("Transacción | Fecha de la transacción | Total de compra", fontContenido), BaseColor.BLACK, Element.ALIGN_LEFT, 5, 1);
            addCell(new Phrase("Producto", fontContenido), BaseColor.BLACK, Element.ALIGN_CENTER, 0, 1);
            addCell(new Phrase("Cantidad", fontContenido), BaseColor.BLACK, Element.ALIGN_CENTER, 0, 1);
            addCell(new Phrase("Precio", fontContenido), BaseColor.BLACK, Element.ALIGN_CENTER, 0, 1);
            addCell(new Phrase("Total", fontContenido), BaseColor.BLACK, Element.ALIGN_CENTER, 0, 1);
            if ( this.option == 1)
                addCell(new Phrase("Proveedor", fontContenido), BaseColor.BLACK, Element.ALIGN_CENTER, 0, 1);
            else
                addCell(new Phrase("Cajero", fontContenido), BaseColor.BLACK, Element.ALIGN_CENTER, 0, 1);
            
        flushCell();
        
        if ( this.option == 0 )
        {
            for (Object itemO : this.items) {
                SellTransaction item = (SellTransaction) itemO;
                Employee employee;
                setCell(5);
                    addCell(new Phrase(item.getId_sell() + " | " + item.getDate() + " " + item.getTime() + " | " + "$" + String.format("%.2f", item.getTotal()), fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 5, 1);
                    for ( SellDetail detail : item.getSellsDetails() )
                    {
                        employee = new Employee(item.getId_employee());
                        employee.get();
                        Products product = detail.getProduct();
                        product.get();
                        addCell(new Phrase(product.getDescription(), fontContenido), BaseColor.WHITE, Element.ALIGN_CENTER, 0, 1);
                        addCell(new Phrase(Integer.toString(detail.getUnits()), fontContenido), BaseColor.WHITE, Element.ALIGN_CENTER, 0, 1);
                        addCell(new Phrase( "$" + String.format("%.2f", product.getPrice()), fontContenido), BaseColor.WHITE, Element.ALIGN_CENTER, 0, 1);
                        double total = (double) detail.getUnits() * product.getPrice();
                        double discount = (total * (product.getCategory().getDiscount() / 100.00));
                        addCell(new Phrase( "$" + String.format("%.2f", total-discount ), fontContenido), BaseColor.WHITE, Element.ALIGN_CENTER, 0, 1);
                        addCell(new Phrase( employee.getName() + " - " + employee.getUser(), fontContenido), BaseColor.WHITE, Element.ALIGN_CENTER, 0, 1);
                    }
                flushCell();
            }
        }
        else
        {
            for (Object itemO : this.items) {
                BuyTransaction item = (BuyTransaction) itemO;
                Provider provider;
                setCell(5);
                    addCell(new Phrase(item.getId_buy() + " | " + item.getDate() + " " + item.getTime() + " | " + "$" + String.format("%.2f", item.getTotal()), fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 5, 1);
                    for ( BuyDetail detail : item.getBuysDetails() )
                    {
                        provider = new Provider(item.getId_provider());
                        provider.get();
                        Products product = detail.getProduct();
                        product.get();
                        addCell(new Phrase(product.getDescription(), fontContenido), BaseColor.WHITE, Element.ALIGN_CENTER, 0, 1);
                        addCell(new Phrase(Integer.toString(detail.getUnits()), fontContenido), BaseColor.WHITE, Element.ALIGN_CENTER, 0, 1);
                        addCell(new Phrase( "$" + String.format("%.2f", product.getPrice()), fontContenido), BaseColor.WHITE, Element.ALIGN_CENTER, 0, 1);
                        double total = (double) detail.getUnits() * product.getPrice();
                        addCell(new Phrase( "$" + String.format("%.2f", total ), fontContenido), BaseColor.WHITE, Element.ALIGN_CENTER, 0, 1);
                        addCell(new Phrase( provider.getName() + " - " + provider.getOwner(), fontContenido), BaseColor.WHITE, Element.ALIGN_CENTER, 0, 1);
                    }
                flushCell();
            }
        }

        generate();
    }

    private void generate() {

        Document document = new Document(PageSize.A4, 10, 10, 10, 10);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("reports/" + this.id + ".pdf");
            PdfWriter.getInstance(document, fileOutputStream);
            document.open();

            document.add(this.documentRoot);

            document.close();

            File file = new File("reports/" + this.id + ".pdf");
            Desktop.getDesktop().open(file);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
