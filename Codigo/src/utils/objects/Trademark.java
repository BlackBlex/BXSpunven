package utils.objects;

import bin.Main;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;

public class Trademark {

    private int ID;
    private String name, code;

    private final OperationSQL sql = Main.SQL;

    public Trademark(int id) {
        ID = id;
    }

    public void get() {
        if ( ID != 0 )
        {            
            sql.rows.put("name", "code");
            sql.wheres.put(new MultiString("id", Integer.toString(ID)), new MultiString("=", ""));
            MultiString[] result = sql.select("trademark");

            for (MultiString trademark : result) {
                setName(trademark.getOne());
                setCode(trademark.getTwo());
            }
        }
        else
        {
            setName("Sin marca");
            setCode("0");
        }
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return this.code;
    }

    private void setID(int id) {
        //ID = id;
    }

    public void setName(String n) {
        name = n;
    }

    public void setCode(String c) {
        code = c;
    }

    public void save() {
        sql.rows.put("name", this.name);
        sql.rows.put("code", this.code);
        sql.wheres.put(new MultiString("id", Integer.toString(this.ID)), new MultiString("=", ""));
        if (!sql.update("trademark")) {
            sql.insert("trademark");
        }
    }

    public boolean delete() {
        sql.wheres.put(new MultiString("id", Integer.toString(this.ID)), new MultiString("=", ""));
        return sql.delete("trademark");
    }

    @Override
    public String toString() {
        return this.name + " - " + String.format("%0" + Main.config.getString("max_leading_zeros_trademark") + "d", Integer.parseInt(this.code));
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) 
            return false;
        if (!(obj instanceof Trademark)) 
            return false;

        Trademark other= (Trademark) obj;
        if (this.name.equals(other.name)) 
            return true;

        return false;
    }
}
