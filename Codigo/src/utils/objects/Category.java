package utils.objects;

import bin.Main;

import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;

public class Category {

    private int ID;
    private String name;
    private double discount;

    private final OperationSQL sql = Main.SQL;

    public Category(int id) {
        ID = id;
    }

    public void get() {
        if ( this.ID > 0 )
        {
            sql.rows.put("name", "discount");
            sql.wheres.put(new MultiString("id", Integer.toString(ID)), new MultiString("=", ""));
            MultiString[] result = sql.select("categories");

            for (MultiString category : result) {
                setName(category.getOne());
                setDiscount(Double.parseDouble(category.getTwo()));
            }
        }
        else
        {
            setName("Sin categoria");
            setDiscount(0.0);
        }
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public double getDiscount() {
        return discount;
    }

    public void setID(int id) {
        //ID = id;
    }

    public void setName(String n) {
        name = n;
    }

    public void setDiscount(double d) {
        discount = d;
    }

    public void save() {
        sql.rows.put("name", name);
        sql.rows.put("discount", Double.toString(discount));
        sql.wheres.put(new MultiString("id", Integer.toString(ID)), new MultiString("=", ""));
        if (!sql.update("categories")) {
            sql.insert("categories");
        }
    }

    public boolean delete() {
        sql.wheres.put(new MultiString("id", Integer.toString(this.ID)), new MultiString("=", ""));
        return sql.delete("categories");
    }

    @Override
    public String toString() {
        return this.name;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) 
            return false;
        if (!(obj instanceof Category)) 
            return false;

        Category other= (Category) obj;
        if (this.name.equals(other.name)) 
            return true;

        return false;
    }
}
