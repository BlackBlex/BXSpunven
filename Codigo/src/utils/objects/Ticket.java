package utils.objects;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class Ticket {

    private float pageWidth = 0;
    private float pageHeight = 0;
    private PdfPTable documentRoot;
    //private Document documentRooot;

    private Paragraph paragraph = null;
    private PdfPCell cell = null;
    private PdfPTable table = null;
    
    private int option;

    private String name,
            street,
            street2,
            id,
            time,
            date,
            cajero,
            cliente,
            subtotal,
            recibido,
            descuento,
            total,
            cambio;

    private ArrayList<ArrayList<String>> items;

    private Font fontTitle = FontFactory.getFont(FontFactory.COURIER_BOLD.toString(), 12, Font.NORMAL);
    private Font fontContenido = FontFactory.getFont(FontFactory.COURIER.toString(), 11, Font.NORMAL);

    public Ticket(int o, String n, String s, String s2, String i, String d, String t, String c, String cus,String subto, String desc, String to, String re, String ca, ArrayList<ArrayList<String>> it) {
        this.option = o;
        this.name = n;
        this.street = s;
        this.street2 = s2;
        this.id = i;
        this.time = t;
        this.date = d;
        this.cajero = c;
        this.cliente = cus;
        this.subtotal = subto;
        this.descuento = desc;
        this.total = to;
        this.recibido = re;
        this.cambio = ca;
        this.items = it;

        this.pageWidth = 220;
        this.pageHeight = 550;

        this.documentRoot = new PdfPTable(1);
        this.documentRoot.setTotalWidth(this.pageWidth - 20);
        this.documentRoot.setLockedWidth(true);
    }

    private void setParagraph() {
        this.paragraph = new Paragraph();
    }

    private void addParagraph(Phrase pa) {
        if (this.paragraph != null) {
            this.paragraph.add(pa);
        }
    }

    private void flushParagraph() {
        if (this.paragraph != null) {
            PdfPCell cell = new PdfPCell(this.paragraph);
            cell.setBorderColor(BaseColor.WHITE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            this.documentRoot.addCell(cell);
        }
    }

    private void setCell(int t) {
        this.table = new PdfPTable(t);
        this.table.getDefaultCell().setBorderWidth(0);
        this.documentRoot.getDefaultCell().setBorderWidth(0);
        this.documentRoot.getDefaultCell().setBorderColor(BaseColor.WHITE);
    }

    private void addCell(Phrase pa, BaseColor co, int h, int col) {
        if (this.table != null) {
            this.cell = new PdfPCell(pa);
            this.cell.setColspan(col);
            this.cell.setFixedHeight(15);
            this.cell.setBorderColor(co);
            this.cell.setBorderWidth(0);
            this.cell.setHorizontalAlignment(h);
            this.table.addCell(this.cell);
        }
    }

    private void flushCell() {
        if (this.table != null) {
            this.documentRoot.addCell(this.table);
        }
    }

    public void process() {
        setParagraph();
            addParagraph(new Phrase(Chunk.NEWLINE));
            addParagraph(new Phrase(Chunk.NEWLINE));
            addParagraph(new Phrase(this.name, fontTitle));
            addParagraph(new Phrase(Chunk.NEWLINE));
            addParagraph(new Phrase(this.street, fontContenido));
            addParagraph(new Phrase(Chunk.NEWLINE));
            addParagraph(new Phrase(this.street2, fontContenido));
            addParagraph(new Phrase(Chunk.NEWLINE));
            addParagraph(new Phrase(Chunk.NEWLINE));
            addParagraph(new Phrase(Chunk.NEWLINE));
        flushParagraph();

        setCell(4);
            addCell(new Phrase("Folio:", fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 2);
            addCell(new Phrase(this.id, fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 5);
            addCell(new Phrase("Fecha:", fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 2);
            addCell(new Phrase(this.date + " " + this.time, fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 5);
            addCell(new Phrase("Cajero:", fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 2);
            addCell(new Phrase(this.cajero, fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 5);
            
            if ( this.option == 0 )
                addCell(new Phrase("Cliente:", fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 2);
            else
                addCell(new Phrase("Proveedor:", fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 2);
                
            addCell(new Phrase("", fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 7);
            addCell(new Phrase(this.cliente, fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 5);
        flushCell();

        setParagraph();
            addParagraph(new Phrase("=============================", fontContenido));
        flushParagraph();

        setCell(3);
            addCell(new Phrase("Producto", fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 3);
            addCell(new Phrase("Cantidad", fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 0);
            addCell(new Phrase("Precio", fontContenido), BaseColor.WHITE, Element.ALIGN_RIGHT, 0);
            addCell(new Phrase("Total", fontContenido), BaseColor.WHITE, Element.ALIGN_RIGHT, 0);
        flushCell();

        setParagraph();
        addParagraph(new Phrase("=============================", fontContenido));
        flushParagraph();
        for (ArrayList<String> item : this.items) {
            setCell(3);
                addCell(new Phrase(item.get(0), fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 3);
                addCell(new Phrase(item.get(1), fontContenido), BaseColor.WHITE, Element.ALIGN_CENTER, 0);
                addCell(new Phrase( "$" + String.format("%.2f", Double.parseDouble(item.get(2))), fontContenido), BaseColor.WHITE, Element.ALIGN_RIGHT, 0);
                addCell(new Phrase( "$" + String.format("%.2f", Double.parseDouble(item.get(3))), fontContenido), BaseColor.WHITE, Element.ALIGN_RIGHT, 0);
            flushCell();
        }

        setParagraph();
            addParagraph(new Phrase("==========================", fontContenido));
            addParagraph(new Phrase(Chunk.NEWLINE));
            addParagraph(new Phrase(Chunk.NEWLINE));
        flushParagraph();

        setCell(4);
            addCell(new Phrase("Subtotal:", fontContenido), BaseColor.WHITE, Element.ALIGN_RIGHT, 2);
            addCell(new Phrase("$" + String.format("%.2f", Double.parseDouble(this.subtotal)), fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 2);
            addCell(new Phrase("Descuento:", fontContenido), BaseColor.WHITE, Element.ALIGN_RIGHT, 2);
            addCell(new Phrase("$" + String.format("%.2f", Double.parseDouble(this.descuento)), fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 2);
            addCell(new Phrase("Total:", fontContenido), BaseColor.WHITE, Element.ALIGN_RIGHT, 2);
            addCell(new Phrase("$" + String.format("%.2f", Double.parseDouble(this.total)), fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 2);
            if ( this.option == 0 )
            {
                addCell(new Phrase("Recibido:", fontContenido), BaseColor.WHITE, Element.ALIGN_RIGHT, 2);
                addCell(new Phrase("$" + String.format("%.2f", Double.parseDouble(this.recibido)), fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 2);
                addCell(new Phrase("Cambio:", fontContenido), BaseColor.WHITE, Element.ALIGN_RIGHT, 2);
                addCell(new Phrase("$" + String.format("%.2f", Double.parseDouble(this.cambio)), fontContenido), BaseColor.WHITE, Element.ALIGN_LEFT, 2);
            }
        flushCell();

        setParagraph();
            addParagraph(new Phrase(Chunk.NEWLINE));
            addParagraph(new Phrase(Chunk.NEWLINE));
            
            if ( this.option == 0 )
                addParagraph(new Phrase("Gracias por su compra  ¡vuelva pronto!", fontContenido));
            else
                addParagraph(new Phrase("Gracias por su venta   ¡vuelva pronto!", fontContenido));
            
            addParagraph(new Phrase(Chunk.NEWLINE));
            addParagraph(new Phrase(Chunk.NEWLINE));
            addParagraph(new Phrase(Chunk.NEWLINE));
            addParagraph(new Phrase(Chunk.NEWLINE));
        flushParagraph();

        generate();
    }

    private void generate() {

        String pathTicket = this.id + ".pdf";
        
        if ( this.option == 0 )
            pathTicket = "tickets/sells/" + pathTicket;
        else
            pathTicket = "tickets/buys/" + pathTicket;
        
        this.pageHeight = this.documentRoot.calculateHeights();

        Document document = new Document(new Rectangle(this.pageWidth, this.pageHeight), 0, 0, 0, 0);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(pathTicket);
            PdfWriter.getInstance(document, fileOutputStream);
            document.open();

            document.add(this.documentRoot);

            document.close();

            File file = new File(pathTicket);
            Desktop.getDesktop().open(file);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
