package utils.objects;

import bin.Main;
import com.blackblex.libs.main.Init;

import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;
import com.blackblex.libs.system.utils.Security;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Employee {

    private boolean ERROR_ID,
            ERROR_NAME,
            ERROR_LASTNAME,
            ERROR_USER,
            ERROR_EMAIL,
            ERROR_PASS,
            ERROR_RANK;
    
    private int ID;
    private String name,
            lastname,
            user,
            email,
            password;
    
    private Rank rank;
    
    private final OperationSQL sql = Main.SQL;

    public Employee(int id) {
        ID = id;
    }

    public void get() {
        sql.rows.put("name", "lastname");
        sql.rows.put("user", "email");
        sql.rows.put("pass", "rank");
        sql.wheres.put(new MultiString("id", Integer.toString(ID)), new MultiString("=", ""));
        MultiString[] result = sql.select("employees");
        MultiString person = result[0];
        
        setName(person.getOne());
        setLastname(person.getTwo());
        setUser(person.getThree());
        setEmail(person.getFour());
        setPassword(person.getFive());
        setRank(new Rank(Integer.parseInt(person.getSix())));
        getRank().get();
        
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getUser() {
        return user;
    }

    public String getEmail() {
        return email;
    }
    
    public String getPassword()
    {
        return password;
    }

    public Rank getRank()
    {
        return rank;
    }
    
    public String getRankName()
    {
        return getRank().getName();
    }
    
    public String getAvatar()
    {
        return "resources\\img\\employees\\employee.png";
    }
    
    public void setName(String n) {
        name = n;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String pass) {
        this.password = pass;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }
    
    public void resetError() {
        this.ERROR_ID = false;
        this.ERROR_NAME = false;
        this.ERROR_LASTNAME = false;
        this.ERROR_USER = false;
        this.ERROR_EMAIL = false;
        this.ERROR_PASS = false;
        this.ERROR_RANK = false;
    }

    public boolean isErrors() {
        int count = 0;

        if (this.ERROR_ID) {
            count++;
        }

        if (this.ERROR_NAME) {
            count++;
        }

        if (this.ERROR_LASTNAME) {
            count++;
        }

        if (this.ERROR_USER) {
            count++;
        }

        if (this.ERROR_EMAIL) {
            count++;
        }

        if (this.ERROR_PASS) {
            count++;
        }
        
        if (this.ERROR_RANK) {
            count++;
        }

        if (count >= 1) {
            return true;
        }

        return false;

    }

    public boolean save() {

        if (this.name.length() < 3) {
            this.ERROR_NAME = true;
        }

        if (this.lastname.length() < 3) {
            this.ERROR_LASTNAME = true;
        }

        if (this.user.length() < 3) {
            this.ERROR_USER = true;
        }

        String regex = "^(.+)@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(this.email);
        if (!matcher.matches()) {
            this.ERROR_EMAIL = true;
        }

        if (this.password.length() < 3) {
            this.ERROR_PASS = true;
        }

        if (this.rank.getName().isEmpty() ) {
            this.ERROR_RANK = true;
        }

        if (!isErrors()) {
            
            Security sec = new Security();
            sec.addKey("BlackBlex");
            sql.rows.put("name", this.name);
            sql.rows.put("lastname", this.lastname);
            sql.rows.put("user", this.user);
            sql.rows.put("email", this.email);
            sql.rows.put("pass", sec.encriptar(this.password));
            sql.rows.put("rank", Integer.toString(this.rank.getID()));
            sql.wheres.put(new MultiString("id", Integer.toString(this.ID)), new MultiString("=", ""));

            if (!sql.update("employees")) {
                sql.insert("employees");
            }

            sql.clearAll();
            return true;
        } else {
            if (this.ERROR_ID) {
                Init.MSG.showMessageError("El codigo del empleado parece ser invalido!.\nRecuerda que solo se permiten numeros.");
            }

            if (this.ERROR_NAME) {
                Init.MSG.showMessageError("El nombre del empleado parece ser invalido!.\nRecuerda que no debe estar en blanco.");
            }

            if (this.ERROR_LASTNAME) {
                Init.MSG.showMessageError("Los apellidos del empleado parece ser invalido!.\nRecuerda que no debe estar en blanco.");
            }

            if (this.ERROR_USER) {
                Init.MSG.showMessageError("El usuario del empleado parece ser invalido!.\nRecuerda que no debe estar en blanco.");
            }

            if (this.ERROR_EMAIL) {
                Init.MSG.showMessageError("El email del empleado parece ser invalido!.\nRecuerda de poner un dominio valido.");
            }

            if (this.ERROR_PASS) {
                Init.MSG.showMessageError("La contraseña del empleado parece ser debil!.\nRecuerda de poner una contraseña mas segura.");
            }

            if (this.ERROR_RANK) {
                Init.MSG.showMessageError("El rango del empleado parece ser incorrecto!.");
            }

            resetError();

            return false;
        }

    }
    
    public boolean delete() {
        sql.wheres.put(new MultiString("id", Integer.toString(this.ID)), new MultiString("=", ""));
        return sql.delete("employees");
    }

    @Override
    public String toString() {
        return this.name + " - " + this.user;
    }

}
