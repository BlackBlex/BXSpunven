package utils.objects;

import bin.Main;
import java.sql.Date;
import java.sql.Time;

import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;
import java.util.ArrayList;

public class BuyTransaction {
    
    public String id_buy;    
    private Date date;    
    private Time time;    
    private double total;
    private int id_provider;
    private ArrayList<BuyDetail> buysdetails = new ArrayList();
    private OperationSQL sql = Main.SQL;

    public BuyTransaction(String id_buy) {
        this.id_buy = id_buy;
    }

    public void get() {
        this.sql.rows.put("date", "time");
        this.sql.rows.put("total", "id_provider");
        
        this.sql.wheres.put(new MultiString("id_buy", this.id_buy), new MultiString("=", ""));
        MultiString[] result = this.sql.select("buys");

        for (MultiString buy : result) {
            setDate(this.sql.formatDate(buy.getOne()));
            setTime(this.sql.formatTime(buy.getTwo()));
            setTotal(Double.parseDouble(buy.getThree()));
            setId_provider(Integer.parseInt(buy.getFour()));
        }
        
        this.sql.rows.put("units", "id_product");
        this.sql.wheres.put(new MultiString("id_buy", this.id_buy), new MultiString("=", ""));
        result = this.sql.select("buys_details");
        
        for ( MultiString buy_detail : result )
        {
            this.buysdetails.add(new BuyDetail(Integer.parseInt(buy_detail.getOne()), new Products(buy_detail.getTwo()) ));
        }
        
    }

    public String getId_buy() {
        return id_buy;
    }

    public int getId_provider() {
        return this.id_provider;
    }

    public Time getTime() {
        return this.time;
    }

    public Date getDate() {
        return this.date;
    }

    public double getTotal() {
        return this.total;
    }
    
    public ArrayList<BuyDetail> getBuysDetails()
    {
        return this.buysdetails;
    }
    
    public void setId_provider(int id_provider) {
        this.id_provider = id_provider;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
    public void setBuyDetail(int unit, Products product)
    {
        this.buysdetails.add(new BuyDetail(unit, product));
    }

    public void save() {
        this.sql.rows.put("date", this.date.toString());
        this.sql.rows.put("time", this.time.toString());
        this.sql.rows.put("total", Double.toString(this.total));
        this.sql.rows.put("id_provider", Integer.toString(this.id_provider));
        this.sql.rows.put("id_buy", this.id_buy);
        this.sql.insert("buys");
        
        for ( BuyDetail buydetail : getBuysDetails() )
        {
            this.sql.rows.put("units", Integer.toString(buydetail.getUnits()));
            this.sql.rows.put("id_product", buydetail.getProduct().getID());
            this.sql.rows.put("id_buy", this.id_buy);
            this.sql.insert("buys_details");
        }
    }

    class BuyDetail
    {
        private int units;
        private Products product;
        
        public BuyDetail(int unit, Products product)
        {
            this.units = unit;
            this.product = product;
        }
        
        public int getUnits()
        {
            return this.units;
        }
        
        public Products getProduct()
        {
            return this.product;
        }
    }
    
}
