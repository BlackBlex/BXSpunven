package utils.objects;

import bin.Main;
import com.blackblex.libs.main.Init;

import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;

public class Products {

    private boolean ERROR_ID,
            ERROR_STOCK,
            ERROR_CATEGORY,
            ERROR_PRICE,
            ERROR_DESCRIPTION,
            ERROR_MODEL,
            ERROR_PRESENTATION,
            ERROR_TRADEMARK;

    private String description,
            model,
            presentation,
            ID,
            oldID = "";

    private double price;
    private int stock;

    private Category category;
    private Trademark trademark;

    private final OperationSQL sql = Main.SQL;

    public Products(String id) {
        ID = id;
    }

    public void get() {
        sql.rows.put("description", "model");
        sql.rows.put("presentation", "price");
        sql.rows.put("stock", "id_category");
        sql.rows.put("id_trademark", "");
        sql.wheres.put(new MultiString("id", ID), new MultiString("=", ""));
        MultiString[] result = sql.select("products");

        for (MultiString item : result) {
            setDescription(item.getOne());
            setModel(item.getTwo());
            setPresentation(item.getThree());
            setPrice(Double.parseDouble(item.getFour()));
            setStock(Integer.parseInt(item.getFive()));
            Category cat = new Category(Integer.parseInt(item.getSix()));
            cat.get();
            setCategory(cat);
            Trademark tra = new Trademark(Integer.parseInt(item.getSeven()));
            tra.get();
            setTrademark(tra);
        }
    }

    public String getID() {
        return ID;
    }

    public int getStock() {
        return stock;
    }

    public Category getCategory() {
        return category;
    }

    public Double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getModel() {
        return model;
    }

    public String getPresentation() {
        return presentation;
    }

    public Trademark getTrademark() {
        return this.trademark;
    }

    public void setID(String iD) {
        oldID = ID;
        ID = iD;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public void setTrademark(Trademark tm) {
        this.trademark = tm;
    }

    public void resetError() {
        ERROR_ID = false;
        ERROR_STOCK = false;
        ERROR_CATEGORY = false;
        ERROR_PRICE = false;
        ERROR_DESCRIPTION = false;
        ERROR_MODEL = false;
        ERROR_PRESENTATION = false;
        ERROR_TRADEMARK = false;
    }

    public boolean isErrors() {
        int count = 0;

        if (ERROR_ID) {
            //count++;
        }

        if (ERROR_STOCK) {
            count++;
        }

        if (ERROR_CATEGORY) {
            count++;
        }

        if (ERROR_PRICE) {
            count++;
        }

        if (ERROR_DESCRIPTION) {
            count++;
        }

        if (ERROR_MODEL) {
            count++;
        }

        if (ERROR_PRESENTATION) {
            count++;
        }

        if (ERROR_TRADEMARK) {
            count++;
        }

        if (count >= 1) {
            return true;
        }

        return false;

    }

    public boolean save() {

        if (description.length() < 3) {
            ERROR_DESCRIPTION = true;
        }

        if (model.length() < 2) {
            ERROR_MODEL = true;
        }

        if (presentation.length() < 3) {
            ERROR_PRESENTATION = true;
        }

        if (price < 0) {
            ERROR_PRICE = true;
        }

        if (stock < 0) {
            ERROR_STOCK = true;
        }

        if (this.category.getID() == 0) {
            ERROR_CATEGORY = true;
        }

        if (this.trademark.getID() == 0) {
            ERROR_TRADEMARK = true;
        }

        sql.wheres.put(new MultiString("id", ID), new MultiString("=", ""));
        MultiString[] selectAll = sql.selectAll("products");

        if (selectAll != null) {
            for (MultiString select : selectAll) {
                if (select.getOne().startsWith(this.ID)) {
                    ERROR_ID = true;
                    break;
                }
            }
        }

        if (!isErrors()) {
            sql.rows.put("description", description);
            sql.rows.put("model", model);
            sql.rows.put("presentation", presentation);
            sql.rows.put("price", Double.toString(price));
            sql.rows.put("stock", Integer.toString(stock));
            sql.rows.put("id_category", Integer.toString(category.getID()));
            sql.rows.put("id_trademark", Integer.toString(trademark.getID()));
            if (oldID.isEmpty()) {
                sql.wheres.put(new MultiString("id", ID), new MultiString("=", ""));
            } else {
                sql.rows.put("id", ID);
                sql.wheres.put(new MultiString("id", oldID), new MultiString("=", ""));
            }
            if (!sql.update("products") && !ERROR_ID) {
                sql.rows.put("id", ID);
                sql.insert("products");
            }
            return true;
        } else {
            if (ERROR_ID) {
                Init.MSG.showMessageError("El ID del producto parece ser invalido o esta duplicado!.");
            }

            if (ERROR_DESCRIPTION) {
                Init.MSG.showMessageError("La descripción del producto parece ser invalido!.\nRecuerda que no debe estar en blanco.");
            }

            if (ERROR_MODEL) {
                Init.MSG.showMessageError("El modelo del producto parece ser invalido!.\nRecuerda que no debe estar en blanco.");
            }

            if (ERROR_PRESENTATION) {
                Init.MSG.showMessageError("La presentación parece ser invalida!.\nRecuerda que no debe estar en blanco.");
            }

            if (ERROR_PRICE) {
                Init.MSG.showMessageError("El precio parece ser invalido!.\nRecuerda que solo se permiten numeros con decimales y no debe ser gratis.");
            }

            if (ERROR_STOCK) {
                Init.MSG.showMessageError("El stock del producto parece estar vacio!.\nRecuerda que debe poner por lo menos un producto en stock.");
            }

            if (ERROR_CATEGORY) {
                Init.MSG.showMessageError("La categoria del producto parece no existir!.\nRecuerda que debes poner una categoria correcta.");
            }

            if (ERROR_TRADEMARK) {
                Init.MSG.showMessageError("La marca del producto parece no existir!.\nRecuerda que debes poner una marca correcta.");
            }

            resetError();

            return false;
        }
    }

    public boolean delete() {
        sql.wheres.put(new MultiString("id", this.ID), new MultiString("=", ""));
        return sql.delete("products");
    }

    @Override
    public String toString() {
        return description + " - " + model;
    }

}
