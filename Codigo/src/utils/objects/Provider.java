package utils.objects;

import bin.Main;
import com.blackblex.libs.main.Init;

import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;

public class Provider extends Person {

    private boolean ERROR_OWNER, ERROR_CODE, ERROR_CATEGORY;

    private String owner, code = "";
    private Category category;

    private final OperationSQL sql = Main.SQL;
    
    public Provider(int id) {
        super(id);
    }
    
    public String getAvatar()
    {
        return "resources\\img\\providers\\providers.png";
    }

    public String getOwner() {
        return this.owner;
    }

    public void setOwner(String o) {
        this.owner = o;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String c) {
        this.code = c;
    }

    public Category getCategory() {
        return this.category;
    }

    public void setCategory(Category c) {
        this.category = c;
    }
    
    public String getCategoryName()
    {
        return getCategory().getName();
    }

    @Override
    public void get() {
        sql.rows.put("name", "owner");
        sql.rows.put("street", "telephone");
        sql.rows.put("id_category", "code");
        sql.wheres.put(new MultiString("id", Integer.toString(ID)), new MultiString("=", ""));
        MultiString[] result = sql.select("providers");

        for (MultiString provider : result) {
            setName(provider.getOne());
            setOwner(provider.getTwo());
            setStreet(provider.getThree());
            setTelephone(Long.parseLong(provider.getFour()));
            
            Category cat = new Category(Integer.parseInt(provider.getFive()));
            cat.get();
            setCategory(cat);
            setCode(provider.getSix());
        }
    }

    @Override
    public void resetError() {
        super.resetError();
        this.ERROR_OWNER = false;
        this.ERROR_CODE = false;
        this.ERROR_CATEGORY = false;
    }

    @Override
    public boolean isErrors() {
        boolean temp = super.isErrors();

        if (temp) {
            return true;
        } else {
            if (this.ERROR_OWNER) {
                return true;
            }
            if (this.ERROR_CODE) {
                return true;
            }
            if (this.ERROR_CATEGORY) {
                return true;
            }
            return false;
        }
    }

    @Override
    public boolean save() {
        boolean temp = super.save();

        if (temp) {
            if (this.owner.length() < 3) {
                this.ERROR_OWNER = true;
            }

            /*if (!Init.isNumeric(this.code, "I")) {
                this.ERROR_CODE = true;
            }*/
            
            if (this.category.getID() == -1) {
                this.ERROR_CATEGORY = true;
            }

            if (!isErrors()) {
                if ( this.code.isEmpty() )
                {
                    sql.rows.put("id", "");
                    sql.order.put("id", "DESC");
                    MultiString ranks[] = sql.selectAll("providers");

                    if ( ranks != null)
                        this.code = Integer.toString(Integer.parseInt(ranks[0].getOne())+1);
                    else
                        this.code = "1";
                }
                
                sql.rows.put("name", name);
                sql.rows.put("owner", owner);
                sql.rows.put("street", street);
                sql.rows.put("telephone", Long.toString(telephone));
                sql.rows.put("code", this.code);
                sql.rows.put("id_category", Integer.toString(this.category.getID()));
                sql.wheres.put(new MultiString("id", Integer.toString(ID)), new MultiString("=", ""));

                if (!sql.update("providers")) {
                    sql.insert("providers");
                }
                return true;
            } else {
                if (this.ERROR_OWNER) {
                    Init.MSG.showMessageError("El dueño parece ser invalido!.\nRecuerda que no debe estar en blanco.");
                }
                if (this.ERROR_CODE) {
                    Init.MSG.showMessageError("El codigo del proveedor parece ser invalido!.\nRecuerda que no debe estar en blanco.");
                }
                if (this.ERROR_CATEGORY) {
                    Init.MSG.showMessageError("La categoria parece ser invalida!.\nRecuerda que no debe estar en blanco.");
                }
                resetError();
                return false;
            }
        } else {
            return false;
        }
    }
    
    public boolean delete() {
        sql.wheres.put(new MultiString("id", Integer.toString(this.ID)), new MultiString("=", ""));
        return sql.delete("providers");
    }
}
