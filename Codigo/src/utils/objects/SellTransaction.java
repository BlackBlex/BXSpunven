package utils.objects;

import bin.Main;
import java.sql.Date;
import java.sql.Time;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;
import java.util.ArrayList;

public class SellTransaction {

    public String id_sell;    
    private Date date;    
    private Time time;    
    private double total;
    private int id_customer, id_employee;
    private ArrayList<SellDetail> sellsdetails = new ArrayList();
    private OperationSQL sql = Main.SQL;
    
    public SellTransaction(String id_sell) {
        this.id_sell = id_sell;
    }

    public void get() {
        
        this.sql.rows.put("date", "time");
        this.sql.rows.put("total", "id_customer");
        this.sql.rows.put("id_employee", "");
        
        this.sql.wheres.put(new MultiString("id_sell", this.id_sell), new MultiString("=", ""));
        MultiString[] result = this.sql.select("sells");

        for (MultiString sell : result) {
            setDate(this.sql.formatDate(sell.getOne()));
            setTime(this.sql.formatTime(sell.getTwo()));
            setTotal(Double.parseDouble(sell.getThree()));
            setId_customer(Integer.parseInt(sell.getFour()));
            setId_employee(Integer.parseInt(sell.getFive()));
        }
        
        this.sql.rows.put("units", "id_product");
        this.sql.rows.put("discount", "");
        this.sql.wheres.put(new MultiString("id_sell", this.id_sell), new MultiString("=", ""));
        result = this.sql.select("sells_details");
        
        for ( MultiString sell_detail : result )
        {
            this.sellsdetails.add(new SellDetail(Integer.parseInt(sell_detail.getOne()), new Products(sell_detail.getTwo()), Double.parseDouble(sell_detail.getThree()) ));
        }
    }

    public String getId_sell() {
        return id_sell;
    }

    public int getId_customer() {
        return this.id_customer;
    }

    public Time getTime() {
        return this.time;
    }

    public Date getDate() {
        return this.date;
    }

    public double getTotal() {
        return this.total;
    }

    public int getId_employee() {
        return this.id_employee;
    }
    
    public ArrayList<SellDetail> getSellsDetails()
    {
        return this.sellsdetails;
    }

    public void setId_customer(int id_customer) {
        this.id_customer = id_customer;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
    public void setId_employee(int id_employee) {
        this.id_employee = id_employee;
    }
    
    public void setSellDetail(int unit, Products product, double discount)
    {
        this.sellsdetails.add(new SellDetail(unit, product, discount));
    }
    
    public void save() {
        this.sql.rows.put("date", this.date.toString());
        this.sql.rows.put("time", this.time.toString());
        this.sql.rows.put("total", Double.toString(this.total));
        this.sql.rows.put("id_customer", Integer.toString(this.id_customer));
        this.sql.rows.put("id_employee", Integer.toString(this.id_employee));
        this.sql.rows.put("id_sell", this.id_sell);
        this.sql.insert("sells");
        
        for ( SellDetail selldetail : getSellsDetails() )
        {
            this.sql.rows.put("units", Integer.toString(selldetail.getUnits()));
            this.sql.rows.put("id_product", selldetail.getProduct().getID());
            this.sql.rows.put("discount", Double.toString( selldetail.getDiscount()));
            this.sql.rows.put("id_sell", this.id_sell);
            this.sql.insert("sells_details");
        }
    }

    class SellDetail
    {
        private int units;
        private Products product;
        private double discount;
        
        public SellDetail(int unit, Products product, double discount)
        {
            this.units = unit;
            this.product = product;
            this.discount = discount;
        }
        
        public int getUnits()
        {
            return this.units;
        }
        
        public Products getProduct()
        {
            return this.product;
        }
        
        public double getDiscount()
        {
            return this.discount;
        }
    }
    
}
