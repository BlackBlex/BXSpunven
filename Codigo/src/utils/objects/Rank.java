package utils.objects;

import bin.Main;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;

public class Rank {
    
    private int idRank = 0;
    private String name = "Sin rango";
    private RankPermissions permission = new RankPermissions(this.idRank);

    private final OperationSQL sql = Main.SQL;
    
    public Rank(int idrank)
    {
        this.idRank = idrank;
    }
    
    public void get()
    {
        if ( this.idRank != -1 )
        {
            sql.rows.put("name", "");
            sql.wheres.put(new MultiString("id", Integer.toString(this.idRank)), new MultiString("=", ""));
            MultiString[] result = sql.select("ranks");

            for (MultiString rank : result) {
                setName(rank.getOne());
            }
            
            setPermission(new RankPermissions(this.idRank));
            getPermission().get();
        }
    }
    
    public int getID()
    {
        return this.idRank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RankPermissions getPermission() {
        return permission;
    }

    public void setPermission(RankPermissions permission) {
        this.permission = permission;
    }
    
    public void save()
    {
        sql.rows.put("name", this.name);
        sql.wheres.put(new MultiString("id", Integer.toString(this.idRank)), new MultiString("=", ""));
        if (!sql.update("ranks")) {
            sql.insert("ranks");
        }
        
        getPermission().save();
    }
    
    public boolean delete() {
        sql.wheres.put(new MultiString("id", Integer.toString(this.idRank)), new MultiString("=", ""));
        return sql.delete("ranks");
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) 
            return false;
        if (!(obj instanceof Rank)) 
            return false;

        Rank other= (Rank) obj;
        if (this.name.equals(other.name)) 
            return true;

        return false;
    }
    
}
