
package utils.objects;

import bin.Main;
import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;

public class RankPermissions {
    
    private int idRank = 0,
                id = 0;
    private boolean
            can_sells = false,
            can_buys = false,
            can_products = false,
            can_reports = false,
            can_employees = false,
            can_providers = false,
            can_customers = false,
            can_trademarks = false,
            can_categories = false,
            can_settings = false;
    
    private final OperationSQL sql = Main.SQL;
    
    public RankPermissions(int idrank)
    {
        this.idRank = idrank;
    }
    
    public void get()
    {
        if ( this.idRank != -1 )
        {
            sql.wheres.put(new MultiString("id_rank", Integer.toString(this.idRank)), new MultiString("=", ""));
            MultiString[] result = sql.selectAll("ranks_permissions");

            for (MultiString permission : result) {
                setID(Integer.parseInt(permission.getOne()));
                setCan_sells(Boolean.parseBoolean(permission.getThree()));
                setCan_buys(Boolean.parseBoolean(permission.getFour()));
                setCan_products(Boolean.parseBoolean(permission.getFive()));
                setCan_reports(Boolean.parseBoolean(permission.getSix()));
                setCan_employees(Boolean.parseBoolean(permission.getSeven()));
                setCan_providers(Boolean.parseBoolean(permission.getEight()));
                setCan_customers(Boolean.parseBoolean(permission.getNine()));
                setCan_settings(Boolean.parseBoolean(permission.getTeen()));
                setCan_trademarks(Boolean.parseBoolean(permission.getEleven()));
                setCan_categories(Boolean.parseBoolean(permission.getTwelve()));
            }
        }
    }
    
    public void setID(int id)
    {
        this.id = id;
    }

    public boolean isCan_sells() {
        return can_sells;
    }

    public void setCan_sells(boolean can_sells) {
        this.can_sells = can_sells;
    }

    public boolean isCan_buys() {
        return can_buys;
    }

    public void setCan_buys(boolean can_buy) {
        this.can_buys = can_buy;
    }

    public boolean isCan_products() {
        return can_products;
    }

    public void setCan_products(boolean can_products) {
        this.can_products = can_products;
    }

    public boolean isCan_reports() {
        return can_reports;
    }

    public void setCan_reports(boolean can_reports) {
        this.can_reports = can_reports;
    }

    public boolean isCan_employees() {
        return can_employees;
    }

    public void setCan_employees(boolean can_employees) {
        this.can_employees = can_employees;
    }

    public boolean isCan_providers() {
        return can_providers;
    }

    public void setCan_providers(boolean can_providers) {
        this.can_providers = can_providers;
    }

    public boolean isCan_customers() {
        return can_customers;
    }

    public void setCan_customers(boolean can_customers) {
        this.can_customers = can_customers;
    }

    public boolean isCan_settings() {
        return can_settings;
    }

    public void setCan_settings(boolean can_settings) {
        this.can_settings = can_settings;
    }
    
    public boolean isCan_trademarks() {
        return can_trademarks;
    }

    public void setCan_trademarks(boolean can_trademarks) {
        this.can_trademarks = can_trademarks;
    }

    public boolean isCan_categories() {
        return can_categories;
    }

    public void setCan_categories(boolean can_categories) {
        this.can_categories = can_categories;
    }
    
    public void save() {
        if ( this.idRank == 0 )
        {
            sql.rows.put("id", "");
            sql.order.put("id", "DESC");
            MultiString ranks[] = sql.selectAll("ranks");
            
            this.idRank = Integer.parseInt(ranks[0].getOne());
        }
        sql.rows.put("id_rank", Integer.toString(this.idRank));
        sql.rows.put("can_sells", Boolean.toString(isCan_sells()));
        sql.rows.put("can_buys", Boolean.toString(isCan_buys()));
        sql.rows.put("can_products", Boolean.toString(isCan_products()));
        sql.rows.put("can_reports", Boolean.toString(isCan_reports()));
        sql.rows.put("can_employees", Boolean.toString(isCan_employees()));
        sql.rows.put("can_providers", Boolean.toString(isCan_providers()));
        sql.rows.put("can_customers", Boolean.toString(isCan_customers()));
        sql.rows.put("can_settings", Boolean.toString(isCan_settings()));
        sql.rows.put("can_trademarks", Boolean.toString(isCan_trademarks()));
        sql.rows.put("can_categories", Boolean.toString(isCan_categories()));
        sql.wheres.put(new MultiString("id", Integer.toString(this.id)), new MultiString("=", ""));
        if (!sql.update("ranks_permissions")) {
            sql.insert("ranks_permissions");
        }
    }
    
    public boolean delete() {
        sql.wheres.put(new MultiString("id", Integer.toString(this.id)), new MultiString("=", ""));
        return sql.delete("ranks_permissions");
    }
}
