package utils.objects;

import bin.Main;
import com.blackblex.libs.main.Init;

import com.blackblex.libs.system.sqlite.OperationSQL;
import com.blackblex.libs.system.utils.MultiString;

public class Person {

    private boolean ERROR_ID,
            ERROR_NAME,
            ERROR_STREET,
            ERROR_TELEPHONE;

    protected int ID;
    protected String name,
            street;
    protected long telephone;

    protected final OperationSQL sql = Main.SQL;

    protected Person(int id) {
        ID = id;
    }

    public void get() {
        sql.rows.put("name", "street");
        sql.rows.put("telephone", "");
        sql.wheres.put(new MultiString("id", Integer.toString(ID)), new MultiString("=", ""));
        MultiString[] result = sql.select("person");

        for (MultiString person : result) {
            setName(person.getOne());
            setStreet(person.getTwo());
            setTelephone(Long.parseLong(person.getThree()));
        }
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public long getTelephone() {
        return telephone;
    }

    public void setName(String n) {
        name = n;
    }

    public void setStreet(String s) {
        street = s;
    }

    public void setTelephone(long t) {
        telephone = t;
    }

    public void resetError() {
        this.ERROR_ID = false;
        this.ERROR_NAME = false;
        this.ERROR_STREET = false;
        this.ERROR_TELEPHONE = false;
    }

    public boolean isErrors() {
        int count = 0;

        if (this.ERROR_ID) {
            count++;
        }

        if (this.ERROR_NAME) {
            count++;
        }

        if (this.ERROR_TELEPHONE) {
            count++;
        }

        if (this.ERROR_STREET) {
            count++;
        }

        if (count >= 1) {
            return true;
        }

        return false;

    }

    public boolean save() {

        if (this.name.length() < 3) {
            this.ERROR_NAME = true;
        }

        if (this.street.length() < 3) {
            this.ERROR_STREET = true;
        }

        if (!Init.isNumeric(Long.toString(this.telephone), "L")) {
            this.ERROR_TELEPHONE = true;
        }

        if (Init.getSize(Long.toString(this.telephone)) < 10 || Init.getSize(Long.toString(this.telephone)) > 10) {
            this.ERROR_TELEPHONE = true;
        }

        if (!isErrors()) {
            return true;
        } else {
            if (this.ERROR_ID) {
                Init.MSG.showMessageError("El codigo del cliente parece ser invalido!.\nRecuerda que solo se permiten numeros.");
            }

            if (this.ERROR_NAME) {
                Init.MSG.showMessageError("El nombre del cliente parece ser invalido!.\nRecuerda que no debe estar en blanco.");
            }

            if (this.ERROR_STREET) {
                Init.MSG.showMessageError("La dirección del cliente parece ser invalida!.\nRecuerda que no debe estar en blanco.");
            }

            if (this.ERROR_TELEPHONE) {
                Init.MSG.showMessageError("El telefono del cliente parece ser invalido!.\nRecuerda que solo se permiten numeros y debe ser de 10 digitos.");
            }

            resetError();

            return false;
        }

    }

    @Override
    public String toString() {
        return this.name + " - " + this.street;
    }

}
