package utils;

import utils.objects.Customer;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class CustomerModel extends AbstractTableModel {

    List items;

    public CustomerModel(List item) {
        this.items = item;
    }
    
    @Override
    public Class getColumnClass(int columnIndex) {
        return Customer.class;
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return "Clientes";
    }

    @Override
    public int getRowCount() {
        return (this.items == null) ? 0 : items.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return (this.items == null) ? null : this.items.get(rowIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    
    public void removeRow(int row) {
        items.remove(row);
        fireTableRowsDeleted(row,row);
    }

}
